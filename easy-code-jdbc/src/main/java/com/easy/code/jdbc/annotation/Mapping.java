package com.easy.code.jdbc.annotation;

import java.lang.annotation.*;

/**
 * 字段-属性映射
 *
 * @author jxfgodlike
 * @date 2022-06-09 22:33
 */
@Documented
@Target({ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Mapping {

    /**
     * 列
     *
     * @return {@code String}
     */
    String column();

    /**
     * 属性
     *
     * @return {@code String}
     */
    String property();
}
