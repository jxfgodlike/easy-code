package com.easy.code.jdbc.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 *
 * @author jxfgodlike
 * @date 2021/11/25
 */
@Data
public class Page implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 列表数据
     */
    private List<?> list;
    /**
     * 当前页数
     */
    private Integer pageNum;
    /**
     * 每页记录数
     */
    private Integer pageSize;
    /**
     * 总记录数
     */
    private Integer total;
    /**
     * 总页数
     */
    private Integer pages;

    /**
     * 分页
     *
     * @param list     列表数据
     * @param pageNum  当前页数
     * @param pageSize 每页记录数
     * @param total    总记录数
     */
    public Page(List<?> list, Integer pageNum, Integer pageSize, Integer total) {
        this.list = list;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.total = total;
        this.pages = (int) Math.ceil((double) total / pageSize);
    }

    /**
     * 分页
     *
     * @param page 页面
     */
    public Page(IPage<?> page) {
        this.list = page.getRecords();
        this.pageNum = (int) page.getCurrent();
        this.pageSize = (int) page.getSize();
        this.total = (int) page.getTotal();
        this.pages = (int) page.getPages();
    }
}
