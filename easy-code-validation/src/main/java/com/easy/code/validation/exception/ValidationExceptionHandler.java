package com.easy.code.validation.exception;

import com.easy.code.core.consts.Symbol;
import com.easy.code.core.enums.HttpStatusEnum;
import com.easy.code.core.resp.EasyResponseBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

/**
 * 验证异常处理程序
 *
 * @author jxfgodlike
 * @date 2022-04-22 23:07
 */
@Slf4j
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ValidationExceptionHandler {

    /**
     * @RequestBosy @Valid entity
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public EasyResponseBody<?> error(MethodArgumentNotValidException e) {
        log.warn(e.getMessage(), e);
        String message = e.getBindingResult().getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(Symbol.COMMA));
        return EasyResponseBody.error(HttpStatusEnum.BAD_REQUEST.code(), message);
    }

    /**
     * @Valid entity
     */
    @ExceptionHandler(BindException.class)
    public EasyResponseBody<?> error(BindException e) {
        log.warn(e.getMessage(), e);
        String message = e.getBindingResult().getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(Symbol.COMMA));
        return EasyResponseBody.error(HttpStatusEnum.BAD_REQUEST.code(), message);
    }

    /**
     * @RequestParam @Valid entity
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public EasyResponseBody<?> error(ConstraintViolationException e) {
        log.warn(e.getMessage(), e);
        String message = e.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(Symbol.COMMA));
        return EasyResponseBody.error(HttpStatusEnum.BAD_REQUEST.code(), message);
    }
}
