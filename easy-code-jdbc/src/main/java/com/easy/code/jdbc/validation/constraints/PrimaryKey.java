package com.easy.code.jdbc.validation.constraints;

import com.easy.code.jdbc.validation.validator.PrimaryKeyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 主键
 *
 * @author jxfgodlike
 * @date 2022-04-15 21:48
 */
@Documented
@Constraint(validatedBy = {PrimaryKeyValidator.class})
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PrimaryKey {

    /**
     * 表名
     */
    String table();

    /**
     * 主键列
     */
    String keyColumn();

    /**
     * where 条件
     */
    String where() default "";

    String message() default "{javax.validation.constraints.PrimaryKey.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
