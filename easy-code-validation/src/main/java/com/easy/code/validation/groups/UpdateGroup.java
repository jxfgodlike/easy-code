package com.easy.code.validation.groups;

import javax.validation.groups.Default;

/**
 * @author jxfgodlike
 * @date 2022-04-15 21:46
 */
public interface UpdateGroup extends Default {
}
