package com.easy.code.core.utils;

import com.easy.code.core.consts.DateFormater;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 转换器
 *
 * @author jxfgodlike
 * @date 2021-12-17 21:00
 */
@Slf4j
public class Converters {

    public static Long toLong(Object value) {
        try {
            return Long.parseLong(String.valueOf(value));
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    public static Integer toInteger(Object value) {
        try {
            return Integer.parseInt(String.valueOf(value));
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    public static Float toFloat(Object value) {
        try {
            return Float.parseFloat(String.valueOf(value));
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    public static Double toDouble(Object value) {
        try {
            return Double.parseDouble(String.valueOf(value));
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    public static BigDecimal toBigDecimal(Object value) {
        try {
            return new BigDecimal(String.valueOf(value));
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    public static Boolean toBoolean(Object value) {
        try {
            return Boolean.parseBoolean(String.valueOf(value));
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    public static LocalDateTime toLocalDateTime(Object value, String pattern) {
        try {
            return LocalDateTime.parse(String.valueOf(value), DateTimeFormatter.ofPattern(pattern));
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    public static LocalDateTime toLocalDateTime(Object value) {
        return toLocalDateTime(value, DateFormater.DATETIME_FORMAT);
    }

    public static LocalDate toLocalDate(Object value, String pattern) {
        try {
            return LocalDate.parse(String.valueOf(value), DateTimeFormatter.ofPattern(pattern));
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    public static LocalDate toLocalDate(Object value) {
        return toLocalDate(value, DateFormater.DATE_FORMAT);
    }

    public static String toStr(Object value) {
        return value == null ? null : value.toString();
    }

    /**
     * list -> tree
     *
     * @param list 列表
     * @return {@code List<T>}
     */
    public static <T extends Tree<T>> List<T> toTree(List<T> list) {
        Map<String, List<T>> listMap = CollUtils.groupByKey(list, e -> StringUtils.isBlank(e.getPid()) ? BigDecimal.ZERO.toString() : e.getPid());
        return list.stream()
                .map(e -> e.setChildren(listMap.get(e.getId())))
                .filter(e -> e.getPid() == null || Objects.equals(e.getPid(), BigDecimal.ZERO.toString()))
                .collect(Collectors.toList());
    }

    /**
     * 合并树id
     *
     * @param sublist 子表
     * @param list    列表
     * @return {@code Set<String>}
     */
    public static <T extends Tree<T>> Set<String> mergeTreeIds(List<T> sublist, List<T> list) {
        Set<String> ids = new HashSet<>();
        for (Tree<T> item : list) {
            for (Tree<T> subItem : sublist) {
                if (Objects.equals(item.getId(), subItem.getId())) {
                    ids.add(subItem.getId());
                    List<T> children = list.stream().filter(childrenItem -> Objects.equals(childrenItem.getPid(), subItem.getId())).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(children)) {
                        ids.addAll(children.stream().map(Tree::getId).collect(Collectors.toList()));
                        ids.addAll(mergeTreeIds(children, list));
                    }
                }
            }
        }
        return ids;
    }
}
