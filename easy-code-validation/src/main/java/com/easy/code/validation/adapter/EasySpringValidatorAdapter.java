package com.easy.code.validation.adapter;

import com.easy.code.validation.constraints.Conditional;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.validator.internal.engine.path.NodeImpl;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 自定义Spring验证器适配器
 *
 * @author jxfgodlike
 * @date 2022-04-16 23:57
 */
public class EasySpringValidatorAdapter extends SpringValidatorAdapter {

    private Validator targetValidator;

    public EasySpringValidatorAdapter(Validator targetValidator) {
        super(targetValidator);
        this.targetValidator = targetValidator;
    }

    public Validator getTargetValidator() {
        return targetValidator;
    }

    public void setTargetValidator(Validator targetValidator) {
        this.targetValidator = targetValidator;
    }

    @Override
    protected void processConstraintViolations(Set<ConstraintViolation<Object>> violations, Errors errors) {
        Set<String> properties = violations.stream()
                .filter(violation -> violation.getConstraintDescriptor().getAnnotation().annotationType().isAssignableFrom(Conditional.class))
                .map(violation -> ((PathImpl) violation.getPropertyPath()).getLeafNode().getName()).collect(Collectors.toSet());

        if (CollectionUtils.isNotEmpty(properties)) {
            violations.removeIf(violation -> {
                NodeImpl leafNode = ((PathImpl) violation.getPropertyPath()).getLeafNode();
                return properties.contains(leafNode.getName()) || properties.contains(leafNode.getParent().getName());
            });
        }
        super.processConstraintViolations(violations, errors);
    }
}
