##引入宏定义
$!{define.vm}
$!{mybatisSupport.vm}

#set($pkg = "mapper")
#set($suffix = "Mapper")

##拿到主键
#set($pk = $tableInfo.pkColumn.get(0))
#set($is_deleted = "is_deleted")
#set($typeHandler = "com.easy.code.jdbc.handler.JSONTypeHandler")

##定义Dao
#set($daoType = $!tableInfo.name + "Dao")

##定义实体对象名
#set($entityType = $!tableInfo.name + "Entity")
#set($entityName = $!tool.firstLowerCase($!tableInfo.name))
#set($resultMap = $!{entityName} + "Map")

##使用宏定义设置回调（保存位置与文件后缀）
#save("/$!{pkg.replace('.', '/')}", "$!{suffix}.xml")

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="$!{tableInfo.savePackageName}.dao.$!daoType">

    <resultMap type="$!{tableInfo.savePackageName}.entity.$!entityType" id="$!resultMap">
#foreach($column in $tableInfo.fullColumn)
        <result property="$!column.name" column="$!column.obj.name" jdbcType="$!column.ext.jdbcType" #if($column.ext.sqlType.equals("json"))typeHandler="$!typeHandler"#end/>
#end
    </resultMap>

    <insert id="insertBatch" keyProperty="$!pk.name" useGeneratedKeys="true">
        INSERT INTO $!{tableInfo.obj.name}(#foreach($column in $tableInfo.fullColumn)$!column.obj.name#if($velocityHasNext), #end#end)
        VALUES
        <foreach collection="$!{entityName}s" item="$!{entityName}" separator=",">
            (#foreach($column in $tableInfo.fullColumn)#{$!{entityName}.$!{column.name}#if($column.ext.sqlType.equals("json")), typeHandler=$!typeHandler#end}#if($velocityHasNext), #end#end)
        </foreach>
    </insert>

    <update id="updateBatchById">
        UPDATE $!{tableInfo.obj.name}
        <set>
#foreach($column in $tableInfo.otherColumn)
            <trim prefix="$!{column.obj.name}=CASE" suffix="END,">
                <foreach collection="$!{entityName}s" item="$!{entityName}">
                    <if test="$!{entityName}.$!{column.name} != null#if($column.type.equals("java.lang.String")) and $!{entityName}.$!{column.name} != ''#end">
                        WHEN $!pk.obj.name = #{$!{entityName}.$!{pk.name}} THEN #{$!{entityName}.$!{column.name}#if($column.ext.sqlType.equals("json")), typeHandler=$!typeHandler#end}
                    </if>
                    <if test="$!{entityName}.$!{column.name} == null#if($column.type.equals("java.lang.String")) or $!{entityName}.$!{column.name} == ''#end">
                        WHEN $!pk.obj.name = #{$!{entityName}.$!{pk.name}} THEN $!{tableInfo.obj.name}.$!{column.obj.name}
                    </if>
                </foreach>
            </trim>
#end
        </set>
        WHERE $!pk.obj.name IN
        <foreach collection="$!{entityName}s" item="$!{entityName}" open="(" separator="," close=")">
            #{$!{entityName}.$!{pk.name}}
        </foreach>
    </update>

    <delete id="deleteBatchByIds">
        UPDATE $!{tableInfo.obj.name} SET $!is_deleted = 1 WHERE $!pk.obj.name IN
        <foreach collection="$!{pk.name}s" item="$!{pk.name}" open="(" separator="," close=")">
            #{$!{pk.name}}
        </foreach>
    </delete>
</mapper>
