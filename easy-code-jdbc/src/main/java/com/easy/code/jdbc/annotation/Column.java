package com.easy.code.jdbc.annotation;

import java.lang.annotation.*;

/**
 * 列
 *
 * @author jxfgodlike
 * @date 2022-06-09 22:33
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {

    /**
     * 值
     *
     * @return {@link String}
     */
    String value();

    /**
     * 类型处理器
     *
     * @return {@link Class}<{@link ?}>
     */
    Class<?> typeHandler();
}
