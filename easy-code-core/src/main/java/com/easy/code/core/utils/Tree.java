package com.easy.code.core.utils;

import java.util.List;

/**
 * 树
 *
 * @author jxfgodlike
 * @date 2022-05-11 20:34
 */
public interface Tree<T extends Tree<T>> {

    /**
     * getId
     *
     * @return java.lang.String
     */
    String getId();

    /**
     * setId
     *
     * @param id id
     * @return void
     */
    T setId(String id);

    /**
     * getPid
     *
     * @return java.lang.String
     */
    String getPid();

    /**
     * setPid
     *
     * @param pid pid
     * @return void
     */
    T setPid(String pid);

    /**
     * getChildren
     *
     * @return java.util.List<T>
     */
    List<T> getChildren();

    /**
     * setChildren
     *
     * @param children 孩子们
     * @return void
     */
    T setChildren(List<T> children);
}
