package com.easy.code.core.consts;

/**
 * 系统常量
 *
 * @author jxfgodlike
 * @date 2021/11/25
 */
public class SystemParameters {

    /**
     * 超级管理员ID
     */
    public static final Long SUPER_ADMIN = 1L;
    /**
     * 系统菜单id
     */
    public static final Long SYSTEM_MENU = 31L;
    /**
     * 短信验证码1
     */
    public static final String SMS_CODE = "1";
    /**
     * 系统默认密码
     */
    public static final String PASSWORD = "123456";
}
