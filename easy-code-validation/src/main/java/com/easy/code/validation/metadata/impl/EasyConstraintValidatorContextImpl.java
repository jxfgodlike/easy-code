package com.easy.code.validation.metadata.impl;

import org.hibernate.validator.internal.engine.constraintvalidation.ConstraintValidatorContextImpl;
import org.hibernate.validator.internal.engine.path.PathImpl;

import javax.validation.ClockProvider;
import javax.validation.metadata.ConstraintDescriptor;
import java.util.List;

/**
 * 自定义约束验证器上下文impl
 *
 * @author jxfgodlike
 * @date 2022-04-17 0:50
 */
public class EasyConstraintValidatorContextImpl extends ConstraintValidatorContextImpl {

    private Object target;

    public EasyConstraintValidatorContextImpl(List<String> methodParameterNames, ClockProvider clockProvider, PathImpl propertyPath, ConstraintDescriptor<?> constraintDescriptor, Object constraintValidatorPayload) {
        super(methodParameterNames, clockProvider, propertyPath, constraintDescriptor, constraintValidatorPayload);
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }
}