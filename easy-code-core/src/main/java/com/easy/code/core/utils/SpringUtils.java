package com.easy.code.core.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Arrays;

/**
 * Spring工具
 *
 * @author jxfgodlike
 * @date 2021/6/27 16:49
 */
public class SpringUtils implements ApplicationContextAware {

    public static final String LOCAL = "local";

    public static final String DEV = "dev";

    public static final String TEST = "test";

    public static final String PROD = "prod";

    private static ApplicationContext CONTEXT;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        SpringUtils.CONTEXT = context;
    }

    public static <T> T getBean(Class<T> clazz) {
        return CONTEXT.getBean(clazz);
    }

    public static <T> T getBean(String name, Class<T> clazz) {
        return CONTEXT.getBean(name, clazz);
    }

    public static Object getBean(String name) {
        return CONTEXT.getBean(name);
    }

    public ApplicationContext getApplicationContext() {
        return CONTEXT;
    }

    public static String[] getActiveProfiles() {
        return CONTEXT.getEnvironment().getActiveProfiles();
    }

    public static boolean active(String active) {
        return Arrays.asList(getActiveProfiles()).contains(active);
    }
}
