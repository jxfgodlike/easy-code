package com.easy.code.core.annotation;

import com.easy.code.core.config.EasyCoreConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用easy-core
 *
 * @author jxfgodlike
 * @date 2022-04-22 22:56
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(EasyCoreConfig.class)
public @interface EnableEasyCore {
}
