package com.easy.code.validation.validator;

import com.easy.code.validation.constraints.EnumValue;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 枚举值验证器
 *
 * @author jxfgodlike
 * @date 2022-04-15 21:47
 */
public class EnumValueValidator implements ConstraintValidator<EnumValue, Object> {

    private Set<String> values;

    @Override
    public void initialize(EnumValue constraintAnnotation) {
        values = Arrays.stream(constraintAnnotation.value()).collect(Collectors.toSet());
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        return values.contains(String.valueOf(value));
    }
}
