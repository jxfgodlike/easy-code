package com.easy.code.core.enums;

/**
 * 逻辑
 *
 * @author jxfgodlike
 * @date 2021-09-24 22:05
 */
public enum Logical {

    /**
     * 且
     */
    AND,
    /**
     * 或
     */
    OR;

    Logical() {
    }
}
