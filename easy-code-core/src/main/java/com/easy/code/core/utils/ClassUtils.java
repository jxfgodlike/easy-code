package com.easy.code.core.utils;

import com.easy.code.core.consts.Alphabet;
import com.easy.code.core.consts.JavaKeywords;
import com.easy.code.core.consts.Symbol;

/**
 * Class工具
 *
 * @author jxfgodlike
 * @date 2022-05-03 23:44
 */
public class ClassUtils {

    /**
     * 获取Class
     *
     * @param classname 全限定类名称
     * @return {@code Class<?>}
     */
    public static Class<?> forName(String classname) {
        switch (classname) {
            case JavaKeywords.BYTE:
                return Byte.TYPE;
            case JavaKeywords.SHORT:
                return Short.TYPE;
            case JavaKeywords.INT:
                return Integer.TYPE;
            case JavaKeywords.LONG:
                return Long.TYPE;
            case JavaKeywords.FLOAT:
                return Float.TYPE;
            case JavaKeywords.DOUBLE:
                return Double.TYPE;
            case JavaKeywords.CHAR:
                return Character.TYPE;
            case JavaKeywords.BOOLEAN:
                return Boolean.TYPE;
            default:
                try {
                    return Class.forName(classname);
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
        }
    }

    /**
     * 获取Class
     *
     * @param classname   类名称
     * @param isArrayType 是否数组类型
     * @return {@code Class<?>}
     */
    public static Class<?> forName(String classname, boolean isArrayType) {
        if (isArrayType) {
            classname = new StringBuilder()
                    .append(Symbol.LEFT_SQUARE_BRACKETS)
                    .append(Alphabet.L)
                    .append(classname)
                    .append(Symbol.SEMICOLON)
                    .toString();
        }
        return forName(classname);
    }
}
