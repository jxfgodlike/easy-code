package com.easy.code.core.utils;

import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.DocsConfig;
import io.github.yedaxia.apidocs.plugin.markdown.MarkdownDocPlugin;

/**
 * japi文档工具
 *
 * @author jxfgodlike
 * @date 2021-12-09 17:34
 */
public class JApiDocsUtils {

    private static final String API_VERSION = "V1.0";

    /**
     * 生成api文档
     */
    public static void generateApiDocs(String moduleName) {
        DocsConfig config = new DocsConfig();
        config.setProjectPath(moduleName);
        config.setProjectName(null);
        config.setApiVersion(API_VERSION);
        config.setDocsPath(null);
        config.setAutoGenerate(Boolean.TRUE);
        config.addPlugin(new MarkdownDocPlugin());
        Docs.buildHtmlDocs(config);
    }
}
