package com.easy.code.jdbc.validation.validator;

import com.easy.code.core.consts.Symbol;
import com.easy.code.core.utils.PropertyUtils;
import com.easy.code.jdbc.annotation.Mapping;
import com.easy.code.jdbc.consts.SQLKeywords;
import com.easy.code.jdbc.validation.constraints.UniqueKey;
import com.easy.code.validation.metadata.impl.EasyConstraintValidatorContextImpl;
import com.google.common.base.CaseFormat;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 唯一键验证器
 *
 * @author jxfgodlike
 * @date 2022-04-15 21:47
 */
public class UniqueKeyValidator implements ConstraintValidator<UniqueKey, Object> {

    /**
     * 表
     */
    private String table;
    /**
     * 主键列
     */
    private String keyColumn;
    /**
     * 校验列
     */
    private String[] columns;
    /**
     * where 条件
     */
    private String where;
    /**
     * 结果图
     */
    private Mapping[] mappings;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void initialize(UniqueKey constraintAnnotation) {
        table = constraintAnnotation.table();
        keyColumn = constraintAnnotation.keyColumn();
        columns = constraintAnnotation.columns();
        where = constraintAnnotation.where();
        mappings = constraintAnnotation.mappings();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        Object target = ((EasyConstraintValidatorContextImpl) constraintValidatorContext).getTarget();
        Map<String, String> propertyMap = Arrays.stream(mappings).collect(Collectors.toMap(Mapping::column, Mapping::property));
        List<String> keywords = Lists.newArrayList(SQLKeywords.SELECT, BigDecimal.ONE.toString(), SQLKeywords.FROM, table, SQLKeywords.WHERE);
        int length = columns.length;
        for (int i = 0; i < length; i++) {
            String column = columns[i];
            keywords.add(column);
            keywords.add(Symbol.EQUAL);
            keywords.add(Symbol.QUESTION);
            if (i < length - 1) {
                keywords.add(SQLKeywords.AND);
            }
        }
        //默认小写下划线转驼峰
        List<Object> values = Arrays.stream(columns)
                .map(column -> {
                    String property = propertyMap.getOrDefault(column, CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, column));
                    return PropertyUtils.getProperty(target, property);
                }).collect(Collectors.toList());
        String keyProperty = propertyMap.getOrDefault(keyColumn, CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, keyColumn));
        Object keyValue = PropertyUtils.getProperty(target, keyProperty);
        if (StringUtils.isNotBlank(Objects.toString(keyValue, Symbol.EMPTY))) {
            keywords.add(SQLKeywords.AND);
            keywords.add(keyColumn);
            keywords.add(Symbol.UNEQUAL);
            keywords.add(Symbol.QUESTION);
            values.add(keyValue);
        }
        if (StringUtils.isNotBlank(where)) {
            keywords.add(SQLKeywords.AND);
            keywords.add(where);
        }
        keywords.add(SQLKeywords.LIMIT);
        keywords.add(BigDecimal.ONE.toString());
        return CollectionUtils.isEmpty(jdbcTemplate.queryForList(Joiner.on(Symbol.SPACE).join(keywords), values.toArray()));
    }
}
