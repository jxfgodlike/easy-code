package com.easy.code.core.exception;

import com.easy.code.core.enums.HttpStatusEnum;
import com.easy.code.core.resp.EasyResponseBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 全局异常处理程序
 * 通配符用**、不能用*
 *
 * @author jxfgodlike
 * @date 2022/01/06
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NoHandlerFoundException.class)
    public EasyResponseBody<?> error(NoHandlerFoundException e) {
        log.warn(e.getMessage(), e);
        return EasyResponseBody.error(HttpStatusEnum.NOT_FOUND);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public EasyResponseBody<?> error(HttpRequestMethodNotSupportedException e) {
        log.warn(e.getMessage(), e);
        return EasyResponseBody.error(HttpStatusEnum.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public EasyResponseBody<?> error(MissingServletRequestParameterException e) {
        log.warn(e.getMessage(), e);
        return EasyResponseBody.error(HttpStatusEnum.BAD_REQUEST.code(), e.getParameterName());
    }

    @ExceptionHandler(EasyException.class)
    public EasyResponseBody<?> error(EasyException e) {
        log.warn(e.getMessage(), e);
        return EasyResponseBody.error(e.code(), e.message());
    }

    @ExceptionHandler(Exception.class)
    public EasyResponseBody<?> error(Exception e) {
        log.error(e.getMessage(), e);
        return EasyResponseBody.error(HttpStatusEnum.ERROR);
    }
}
