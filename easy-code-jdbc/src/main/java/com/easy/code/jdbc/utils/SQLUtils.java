package com.easy.code.jdbc.utils;

import com.easy.code.core.enums.HttpStatusEnum;
import com.easy.code.core.exception.EasyException;
import com.easy.code.jdbc.consts.SQLKeywords;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

/**
 * sql工具
 *
 * @author jxfgodlike
 * @date 2021/11/25
 */
public class SQLUtils {

    /**
     * SQL注入过滤
     *
     * @param sql 待验证sql
     * @return {@code String}
     */
    public static String sqlInject(String sql) {
        if (StringUtils.isNotBlank(sql)) {
            boolean anyMatch = Arrays.stream(SQLKeywords.KEYWORDS).anyMatch(key -> sql.toLowerCase().contains(key));
            if (anyMatch) {
                throw new EasyException(HttpStatusEnum.ILLEGAL_INPUT);
            }
        }
        return sql;
    }
}
