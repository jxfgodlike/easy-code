package com.easy.code.jdbc.handler;

import com.easy.code.core.utils.JSON;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * json类型处理器
 *
 * @author jxfgodlike
 * @date 2022-07-11 1:19
 */
@MappedJdbcTypes({JdbcType.VARCHAR})
public class JSONTypeHandler extends BaseTypeHandler<Object> {

    @Override
    public void setNonNullParameter(PreparedStatement statement, int columnIndex, Object value, JdbcType jdbcType) throws SQLException {
        statement.setString(columnIndex, JSON.toJSONString(value));
    }

    @Override
    public Object getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
        return JSON.parseObject(resultSet.getString(columnName), Object.class);
    }

    @Override
    public Object getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
        return JSON.parseObject(resultSet.getString(columnIndex), Object.class);
    }

    @Override
    public Object getNullableResult(CallableStatement statement, int columnIndex) throws SQLException {
        return JSON.parseObject(statement.getString(columnIndex), Object.class);
    }
}
