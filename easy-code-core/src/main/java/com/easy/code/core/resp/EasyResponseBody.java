package com.easy.code.core.resp;

import com.easy.code.core.enums.HttpStatusEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回数据
 *
 * @author jxfgodlike
 * @date 2021/07/10
 */
@Data
public class EasyResponseBody<T> implements Serializable {

    private static final long serialVersionUID = -2013481598160844575L;
    /**
     * code
     */
    private Integer code;
    /**
     * msg
     */
    private String message;
    /**
     * data
     */
    private T data;

    public Integer code() {
        return code;
    }

    public String message() {
        return message;
    }

    public T data() {
        return data;
    }

    EasyResponseBody(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder ofHttpStatus(HttpStatusEnum httpStatus) {
        return builder().code(httpStatus.code()).message(httpStatus.message());
    }

    public static <T> EasyResponseBody<T> data(T data) {
        return ofHttpStatus(HttpStatusEnum.SUCCESS).build(data);
    }

    public static <T> EasyResponseBody<T> success() {
        return data(null);
    }

    public static <T> EasyResponseBody<T> error(HttpStatusEnum httpStatus) {
        return ofHttpStatus(httpStatus).build(null);
    }

    public static <T> EasyResponseBody<T> error(Integer code, String message) {
        return builder().code(code).message(message).build(null);
    }

    public static class Builder {
        /**
         * code
         */
        private Integer code;
        /**
         * msg
         */
        private String message;

        Builder() {
        }

        public Builder code(Integer code) {
            this.code = code;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public <T> EasyResponseBody<T> build(T data) {
            return new EasyResponseBody<>(this.code, this.message, data);
        }
    }
}
