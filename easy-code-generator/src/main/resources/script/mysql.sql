SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `tb_department`;
CREATE TABLE `tb_department` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `department_name` VARCHAR(255) NULL COMMENT '部门名',
    `is_deleted` INT(11) NOT NULL DEFAULT 0 COMMENT '是否删除 0-否 1-是',
    `created_by` BIGINT(20) NULL COMMENT '创建人',
    `created_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updated_by` BIGINT(20) NULL COMMENT '修改人',
    `updated_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`id`)
) COMMENT '部门';

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `tb_employee`;
CREATE TABLE `tb_employee` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `last_name` VARCHAR(255) NULL COMMENT '姓名',
    `email` VARCHAR(255) NULL COMMENT '邮箱',
    `gender` INT(11) NOT NULL DEFAULT 0 COMMENT '性别 0-女 1-男',
    `department_id` BIGINT(20) NULL COMMENT '部门id',
    `json` JSON NULL COMMENT 'json',
    `is_deleted` INT(11) NOT NULL DEFAULT 0 COMMENT '是否删除 0-否 1-是',
    `created_by` BIGINT(20) NULL COMMENT '创建人',
    `created_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updated_by` BIGINT(20) NULL COMMENT '修改人',
    `updated_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`id`)
) COMMENT '员工';