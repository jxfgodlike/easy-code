package com.easy.code.jdbc.interceptor;

import com.easy.code.core.consts.Symbol;
import com.easy.code.core.utils.ClassUtils;
import com.easy.code.jdbc.annotation.Column;
import com.easy.code.jdbc.annotation.Where;
import com.easy.code.jdbc.consts.SQLKeywords;
import com.easy.code.jdbc.entity.EasyEntity;
import com.easy.code.jdbc.mapper.EasyMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.ibatis.type.TypeHandlerRegistry;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 简单sql注入器
 * org.apache.ibatis.executor.Executor
 * org.apache.ibatis.executor.statement.StatementHandler
 * org.apache.ibatis.executor.parameter.ParameterHandler
 * org.apache.ibatis.executor.resultset.ResultSetHandler
 *
 * @author jxfgodlike
 * @date 2022-10-22 19:54
 */
@Intercepts({
        @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class}),
        @Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})
})
public class EasySqlInjector implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        Object target = invocation.getTarget();
        if (target instanceof Executor) {
            return executorEnhance(invocation);
        }
        if (target instanceof StatementHandler) {
            return statementHandlerEnhance(invocation);
        }
        return invocation.proceed();
    }

    /**
     * 执行器增强
     *
     * @param invocation 调用
     * @return {@link Object}
     * @throws Throwable throwable
     */
    private Object executorEnhance(Invocation invocation) throws Throwable {
        Executor executor = (Executor) invocation.getTarget();
        Object[] args = invocation.getArgs();
        MappedStatement mappedStatement = (MappedStatement) args[0];
        Object parameterObject = args[1];
        if (parameterObject instanceof EasyEntity) {
            EasyEntity entity = (EasyEntity) parameterObject;
            if (Objects.equals(mappedStatement.getSqlCommandType(), SqlCommandType.INSERT)) {
                entity.fillCreateBy(null);
            }
            if (Objects.equals(mappedStatement.getSqlCommandType(), SqlCommandType.UPDATE)) {
                entity.fillUpdateBy(null);
            }
        }
        ParameterizedType easyMapper = filterEasyMapperParameterizedType(mappedStatement);
        if (Objects.isNull(easyMapper)) {
            return invocation.proceed();
        }
        TypeHandlerRegistry typeHandlerRegistry = mappedStatement.getConfiguration().getTypeHandlerRegistry();
        Class<?> entity = ClassUtils.forName(easyMapper.getActualTypeArguments()[0].getTypeName());
        for (Field property : entity.getDeclaredFields()) {
            Column column = property.getAnnotation(Column.class);
            if (Objects.isNull(column)) {
                continue;
            }
            typeHandlerRegistry.register(property.getType(), column.typeHandler());
        }
        return invocation.proceed();
    }

    /**
     * sql语句处理器增强
     *
     * @param invocation 调用
     * @return {@link Object}
     * @throws Throwable throwable
     */
    private Object statementHandlerEnhance(Invocation invocation) throws Throwable {
        StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
        MappedStatement mappedStatement = (MappedStatement) SystemMetaObject.forObject(statementHandler).getValue("delegate.mappedStatement");
        if (Objects.equals(mappedStatement.getSqlCommandType(), SqlCommandType.INSERT)) {
            return invocation.proceed();
        }
        ParameterizedType easyMapper = filterEasyMapperParameterizedType(mappedStatement);
        if (Objects.isNull(easyMapper)) {
            return invocation.proceed();
        }
        Class<?> entity = ClassUtils.forName(easyMapper.getActualTypeArguments()[0].getTypeName());
        Where where = entity.getAnnotation(Where.class);
        if (Objects.isNull(where)) {
            return invocation.proceed();
        }
        BoundSql boundSql = statementHandler.getBoundSql();
        String sql = boundSql.getSql();
        StringBuilder builder = new StringBuilder(sql);
        builder.append(Symbol.SPACE)
                .append(StringUtils.upperCase(sql).contains(SQLKeywords.WHERE) ? SQLKeywords.AND : SQLKeywords.WHERE)
                .append(Symbol.SPACE)
                .append(where.value());
        SystemMetaObject.forObject(boundSql).setValue("sql", builder.toString());
        return invocation.proceed();
    }

    /**
     * 过滤EasyMapper参数化类型
     *
     * @param mappedStatement 映射语句
     * @return {@link ParameterizedType}
     */
    private ParameterizedType filterEasyMapperParameterizedType(MappedStatement mappedStatement) {
        String statementId = mappedStatement.getId();
        Class<?> mapper = ClassUtils.forName(statementId.substring(0, statementId.lastIndexOf(Symbol.CONCAT)));
        Map<? extends Class<?>, ParameterizedType> classParameterizedTypeMap = Arrays.stream(mapper.getGenericInterfaces())
                .map(e -> ((ParameterizedType) e))
                .collect(Collectors.toMap(
                        e -> ClassUtils.forName(e.getRawType().getTypeName()),
                        Function.identity(),
                        (a, b) -> a));
        return classParameterizedTypeMap.get(EasyMapper.class);
    }

    @Override
    public Object plugin(Object target) {
        if (target instanceof Executor) {
            return Plugin.wrap(target, this);
        }
        if (target instanceof StatementHandler) {
            return Plugin.wrap(target, this);
        }
        return target;
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
