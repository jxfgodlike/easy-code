package com.easy.code.jdbc.annotation;

import com.easy.code.jdbc.config.EasyJdbcConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用easy-jdbc
 *
 * @author jxfgodlike
 * @date 2022-04-22 22:56
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(EasyJdbcConfig.class)
public @interface EnableEasyJdbc {
}
