package com.easy.code.generator;

import com.easy.code.core.annotation.EnableEasyCore;
import com.easy.code.jdbc.annotation.EnableEasyJdbc;
import com.easy.code.validation.annotation.EnableEasyValidation;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jxfgodlike
 * @date 2022-06-26 23:12
 */
@EnableEasyCore
@EnableEasyJdbc
@EnableEasyValidation
@SpringBootApplication
public class EasyCodeGeneratorApp {

    public static void main(String[] args) {
        SpringApplication.run(EasyCodeGeneratorApp.class, args);
    }
}
