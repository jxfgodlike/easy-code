package com.easy.code.core.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.ReflectionUtils;

import java.beans.FeatureDescriptor;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 属性工具
 *
 * @author jxfgodlike
 * @date 2022-04-18 20:54
 */
public class PropertyUtils {

    /**
     * 获取属性
     *
     * @param target 目标
     * @param name   名字
     * @return {@code Object}
     */
    public static Object getProperty(Object target, String name) {
        try {
            Field field = target.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return ReflectionUtils.getField(field, target);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 设置属性
     *
     * @param target 目标
     * @param name   名字
     * @param value  价值
     */
    public static void setProperty(Object target, String name, Object value) {
        try {
            Field field = target.getClass().getDeclaredField(name);
            field.setAccessible(true);
            ReflectionUtils.setField(field, target, value);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 复制属性列表忽略null
     *
     * @param sources  来源
     * @param supplier 提供者
     * @return {@code List<T>}
     */
    public static <S, T> List<T> copyPropertiesListIgnoreNull(List<S> sources, Supplier<T> supplier) {
        return copyPropertiesListIgnoreNull(sources, supplier, null);
    }

    /**
     * 复制属性列表忽略null
     *
     * @param sources  来源
     * @param supplier 提供者
     * @param consumer 消费者
     * @return {@code List<T>}
     */
    public static <S, T> List<T> copyPropertiesListIgnoreNull(List<S> sources, Supplier<T> supplier, BiConsumer<S, T> consumer) {
        return sources.stream().map(source -> copyPropertiesIgnoreNull(source, supplier, consumer)).collect(Collectors.toList());
    }

    /**
     * 复制属性忽略null
     *
     * @param source   源
     * @param supplier 提供者
     * @return {@code T}
     */
    public static <T> T copyPropertiesIgnoreNull(Object source, Supplier<T> supplier) {
        return copyPropertiesIgnoreNull(source, supplier, null);
    }

    /**
     * 复制属性忽略null
     *
     * @param source   源
     * @param supplier 提供者
     * @param consumer 消费者
     * @return {@code T}
     */
    public static <S, T> T copyPropertiesIgnoreNull(S source, Supplier<T> supplier, BiConsumer<S, T> consumer) {
        T target;
        if (Objects.isNull(source) || Objects.isNull(supplier) || Objects.isNull(target = supplier.get())) {
            return null;
        }
        BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
        if (Objects.nonNull(consumer)) {
            consumer.accept(source, target);
        }
        return target;
    }

    /**
     * 获取对象null属性名
     *
     * @param source 源
     * @return {@code String[]}
     */
    public static String[] getNullPropertyNames(Object source) {
        if (Objects.isNull(source)) {
            return null;
        }
        BeanWrapper beanWrapper = new BeanWrapperImpl(source);
        return Arrays.stream(beanWrapper.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(name -> Objects.isNull(beanWrapper.getPropertyValue(name)))
                .toArray(String[]::new);
    }
}
