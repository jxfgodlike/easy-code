package com.easy.code.core.consts;

/**
 * 是或否
 *
 * @author jxfgodlike
 * @date 2022-04-22 21:38
 */
public class YesOrNo {

    /**
     * 真
     */
    public static final Integer YES = 1;
    /**
     * 假
     */
    public static final Integer NO = 0;
}
