package com.easy.code.core.consts;

/**
 * 符号常量
 *
 * @author jxfgodlike
 * @date 2022-04-27 22:11
 */
public class Symbol {

    public static final String EMPTY = "";
    public static final String CONCAT = ".";
    public static final String COMMA = ",";
    public static final String COLON = ":";
    public static final String SEMICOLON = ";";
    public static final String QUESTION = "?";
    public static final String STAR = "*";
    public static final String UNDERSCORE = "_";
    public static final String SLASH = "/";
    public static final String BACKSLASH = "\\";
    public static final String HORIZONTAL_BAR = "-";
    public static final String VERTICAL_BAR = "|";
    public static final String SPACE = " ";
    public static final String COMMA_SPACE = ", ";
    public static final String EQUAL = "=";
    public static final String EQUALS = "==";
    public static final String UNEQUAL = "!=";
    public static final String SCOPE_OPERATOR = "::";
    public static final String TAB = "\t";
    public static final String NEWLINE = "\n";
    public static final String LEFT_BRACKETS = "(";
    public static final String RIGHT_BRACKETS = ")";
    public static final String LEFT_SQUARE_BRACKETS = "[";
    public static final String RIGHT_SQUARE_BRACKETS = "]";
    public static final String LEFT_CURLY_BRACKETS = "{";
    public static final String RIGHT_CURLY_BRACKETS = "}";
    public static final String LEFT_ANGLE_BRACKETS = "<";
    public static final String RIGHT_ANGLE_BRACKETS = ">";
    public static final String RIGHT_ARROW = "->";
}
