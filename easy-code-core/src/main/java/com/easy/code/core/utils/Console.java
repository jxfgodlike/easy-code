package com.easy.code.core.utils;

import com.bethecoder.ascii_table.ASCIITable;
import com.easy.code.core.resp.EasyResponseBody;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 控制台
 *
 * @author jxfgodlike
 * @date 2021-11-30 15:10
 */
public class Console {

    private static final String EMPTY = "empty";
    private static final String REQUEST_URL = "Request URL";
    private static final String REQUEST_METHOD = "Request Method";
    private static final String STATUS_CODE = "Status Code";
    private static final String REQUEST_BODY = "Request Body";
    private static final String RESPONSE_BODY = "Response Body";

    public static <T> String of(List<Map<String, T>> data) {
        String[] header = {EMPTY};
        String[][] body = {{EMPTY}};
        if (CollectionUtils.isNotEmpty(data)) {
            header = data.get(0).keySet().toArray(new String[0]);
            body = data.stream()
                    .map(d -> d.values().stream()
                            .map(String::valueOf)
                            .toArray(String[]::new)
                    ).toArray(String[][]::new);
        }
        return ASCIITable.getInstance().getTable(header, body);
    }

    public static <T> String ofMap(Map<String, T> ofMap) {
        return of(Collections.singletonList(ofMap));
    }

    public static String ofEasyResponseBody(String url, String method, Object requestBody, EasyResponseBody<?> responseBody) {
        ImmutableMap<String, String> ofMap = ImmutableMap.of(
                REQUEST_URL, url,
                REQUEST_METHOD, method,
                STATUS_CODE, String.valueOf(responseBody.code()),
                REQUEST_BODY, String.valueOf(requestBody),
                RESPONSE_BODY, String.valueOf(responseBody.data())
        );
        return ofMap(ofMap);
    }
}
