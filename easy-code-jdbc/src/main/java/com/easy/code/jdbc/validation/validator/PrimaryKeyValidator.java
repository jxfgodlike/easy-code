package com.easy.code.jdbc.validation.validator;

import com.easy.code.core.consts.Symbol;
import com.easy.code.jdbc.consts.SQLKeywords;
import com.easy.code.jdbc.validation.constraints.PrimaryKey;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.List;

/**
 * 主键验证器
 *
 * @author jxfgodlike
 * @date 2022-04-15 21:47
 */
public class PrimaryKeyValidator implements ConstraintValidator<PrimaryKey, Object> {

    /**
     * 表
     */
    private String table;
    /**
     * 主键列
     */
    private String keyColumn;
    /**
     * where 条件
     */
    private String where;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void initialize(PrimaryKey constraintAnnotation) {
        table = constraintAnnotation.table();
        keyColumn = constraintAnnotation.keyColumn();
        where = constraintAnnotation.where();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        List<String> keywords = Lists.newArrayList(SQLKeywords.SELECT, BigDecimal.ONE.toString(), SQLKeywords.FROM, table, SQLKeywords.WHERE, keyColumn, Symbol.EQUAL, Symbol.QUESTION);
        if (StringUtils.isNotBlank(where)) {
            keywords.add(SQLKeywords.AND);
            keywords.add(where);
        }
        keywords.add(SQLKeywords.LIMIT);
        keywords.add(BigDecimal.ONE.toString());
        return CollectionUtils.isEmpty(jdbcTemplate.queryForList(Joiner.on(Symbol.SPACE).join(keywords), value));
    }
}
