package com.easy.code.validation.config;

import com.easy.code.validation.adapter.EasySpringValidatorAdapter;
import com.easy.code.validation.exception.ValidationExceptionHandler;
import com.easy.code.validation.metadata.impl.EasyConstraintTreeImpl;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.agent.ByteBuddyAgent;
import net.bytebuddy.dynamic.loading.ClassReloadingStrategy;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;
import org.hibernate.validator.internal.engine.constraintvalidation.ConstraintTree;
import org.springframework.boot.validation.MessageInterpolatorFactory;
import org.springframework.context.annotation.*;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

import javax.annotation.PostConstruct;
import javax.validation.Validator;

/**
 * 验证器配置
 *
 * @author jxfgodlike
 * @date 2022-04-16 0:52
 */
@Configuration
@Import({ValidationExceptionHandler.class})
public class EasyValidationConfig {

    /**
     * 重新加载class
     * 这种方法利用了HotSpot的HotSwap机制，由于无法添加方法或字段，因此机制非常有限
     * 方法代理 ConstraintTree.of() -> EasyConstraintTreeImpl.of()
     */
    @PostConstruct
    public void reloadConstraintTree() {
        ByteBuddyAgent.install();
        Class<? extends ConstraintTree> clazz = new ByteBuddy()
                .redefine(ConstraintTree.class)
                .method(ElementMatchers.named("of"))
                .intercept(MethodDelegation.to(EasyConstraintTreeImpl.class))
                .make()
                .load(ConstraintTree.class.getClassLoader(), ClassReloadingStrategy.fromInstalledAgent())
                .getLoaded();
    }

    @Bean
    public SpelExpressionParser spelExpressionParser() {
        return new SpelExpressionParser();
    }

    /**
     * 默认验证器
     * bean初始化节点
     * Constructor -> setProperty -> BeanNameAware.setBeanName -> BeanFactoryAware.setBeanFactory ->
     * BeanPostProcessor.postProcessBeforeInitialization(@PostConstruct) -> Initialization.afterPropertiesSet ->
     * initMethod -> BeanPostProcessor.postProcessAfterInitialization
     * @return {@code LocalValidatorFactoryBean}
     */
    @Bean
    @Role(2)
    public LocalValidatorFactoryBean defaultValidator() {
        LocalValidatorFactoryBean factoryBean = new LocalValidatorFactoryBean();
        MessageInterpolatorFactory interpolatorFactory = new MessageInterpolatorFactory();
        factoryBean.setMessageInterpolator(interpolatorFactory.getObject());
        return factoryBean;
    }

    @Bean
    @Primary
    public EasySpringValidatorAdapter easySpringValidatorAdapter(SpringValidatorAdapter springValidatorAdapter) {
        Validator targetValidator = springValidatorAdapter.unwrap(null);
        return new EasySpringValidatorAdapter(targetValidator);
    }
}
