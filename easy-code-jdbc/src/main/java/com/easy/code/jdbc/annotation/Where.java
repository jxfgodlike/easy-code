package com.easy.code.jdbc.annotation;

import java.lang.annotation.*;

/**
 * where 条件
 *
 * @author jxfgodlike
 * @date 2022-06-09 22:33
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Where {

    /**
     * 值
     *
     * @return {@link String}
     */
    String value();
}
