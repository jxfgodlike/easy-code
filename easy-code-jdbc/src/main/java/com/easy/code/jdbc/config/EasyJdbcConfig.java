package com.easy.code.jdbc.config;

import com.easy.code.jdbc.exception.JdbcExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * easy-jdbc配置
 *
 * @author jxfgodlike
 * @date 2022-04-23 21:21
 */
@Configuration
@Import({JdbcExceptionHandler.class, JpaConfig.class, MybatisConfig.class, MybatisPlusConfig.class})
public class EasyJdbcConfig {

}
