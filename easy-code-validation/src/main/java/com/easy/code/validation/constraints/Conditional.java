package com.easy.code.validation.constraints;

import com.easy.code.validation.validator.ConditionalValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 条件
 *
 * @author jxfgodlike
 * @date 2022-04-15 21:48
 */
@Documented
@Constraint(validatedBy = {ConditionalValidator.class})
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Conditional {

    String expression();

    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
