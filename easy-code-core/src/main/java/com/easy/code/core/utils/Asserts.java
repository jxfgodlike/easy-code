package com.easy.code.core.utils;

import com.easy.code.core.exception.ParameterException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Objects;

/**
 * 断言
 *
 * @author jxfgodlike
 * @date 2021/11/25
 */
public class Asserts {

    public static void isNull(Object obj, String message) {
        if (Objects.nonNull(obj)) {
            throw new ParameterException(message);
        }
    }

    public static void nonNull(Object obj, String message) {
        if (Objects.isNull(obj)) {
            throw new ParameterException(message);
        }
    }

    public static void yes(boolean expression, String message) {
        if (!expression) {
            throw new ParameterException(message);
        }
    }

    public static void no(boolean expression, String message) {
        if (expression) {
            throw new ParameterException(message);
        }
    }

    public static void isNotBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new ParameterException(message);
        }
    }

    public static void isNotEmpty(Collection<?> collection, String message) {
        if (CollectionUtils.isEmpty(collection)) {
            throw new ParameterException(message);
        }
    }
}
