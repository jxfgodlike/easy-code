package com.easy.code.core.thread;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.concurrent.TimeUnit;

/**
 * 线程池执行人属性
 *
 * @author jxfgodlike
 * @date 2021-08-15 0:22
 */
@Data
@ConfigurationProperties(prefix = "thread.pool")
public class ThreadPoolProperties {

    /**
     * 核心线程数
     */
    private int corePoolSize;
    /**
     * 最大线程数
     */
    private int maximumPoolSize;
    /**
     * 维持时间
     */
    private long keepAliveTime;
    /**
     * 时间单位
     */
    private TimeUnit timeUnit;
    /**
     * 阻塞队列数
     */
    private int workQueueSize;
}
