package com.easy.code.core.consts;

/**
 * 日期格式常量
 *
 * @author jxfgodlike
 * @date 2021/11/24
 */
public class DateFormater {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_ = "yyyy/MM/dd";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATETIME_FORMAT_ = "yyyy/MM/dd HH:mm:ss";
    public static final String DATETIME_FORMAT_CONCAT = "yyyyMMddHHmmss";
}

