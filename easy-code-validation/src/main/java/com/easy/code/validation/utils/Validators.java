package com.easy.code.validation.utils;

import com.easy.code.core.consts.Symbol;
import com.easy.code.core.exception.ParameterException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 验证器
 * hibernate-validator校验工具类
 * 参考文档：http://docs.jboss.org/hibernate/validator/5.4/reference/en-US/html_single/
 *
 * @author jxfgodlike
 * @date 2021/11/25
 */
public class Validators {

    private static final Validator VALIDATOR;

    static {
        VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();
    }

    /**
     * 验证实体
     *
     * @param entity 待校验实体
     * @param groups 待校验的组
     */
    public static void validateEntity(Object entity, Class<?>... groups) {
        Set<ConstraintViolation<Object>> constraintViolations = VALIDATOR.validate(entity, groups);
        String message = constraintViolations.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(Symbol.COMMA));
        throw new ParameterException(message);
    }
}
