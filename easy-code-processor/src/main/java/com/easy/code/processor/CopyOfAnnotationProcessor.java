package com.easy.code.processor;

import com.easy.code.core.consts.JavaKeywords;
import com.easy.code.core.consts.Symbol;
import com.easy.code.core.utils.CollUtils;
import com.easy.code.processor.annotation.CopyOf;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.ArrayCreationLevel;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.nodeTypes.NodeWithName;
import com.github.javaparser.ast.nodeTypes.NodeWithSimpleName;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.Type;
import com.github.javaparser.ast.type.TypeParameter;
import com.google.auto.service.AutoService;
import com.squareup.javapoet.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

/**
 * 复制处理器(jsr269)
 *
 * @author jxfgodlike
 * @date 2022-04-23 22:06
 */
@AutoService({Processor.class})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({"com.easy.code.processor.annotation.CopyOf"})
public class CopyOfAnnotationProcessor extends AbstractProcessor {

    private static final String CLASSFILE = "classfile";
    private static final String ZIP_NAME = "zipName";
    private static final String IMPL = "Impl";
    private static final String $T = "$T";
    private static final String $N = "$N";
    private static final String $L = "$L";
    private static final String $S = "$S";

    private Filer filer;
    private Elements elementUtils;
    private Map<String, String> imports;
    NodeList<TypeParameter> typeParameters = new NodeList<>();
    private String targetPackageName;

    {
        imports = new HashMap<>();
        imports.put("Object", "java.lang");
        imports.put("Class", "java.lang");
        imports.put("Throwable", "java.lang");
        imports.put("ArrayIndexOutOfBoundsException", "java.lang");
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        filer = processingEnv.getFiler();
        elementUtils = processingEnv.getElementUtils();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(CopyOf.class);
        for (Element element : elements) {
            String classname = String.valueOf(element.getSimpleName());
            String packageName = String.valueOf(elementUtils.getPackageOf(element).getQualifiedName());
            String qualifiedName = element.getAnnotation(CopyOf.class).value();
            String classpath = getClass().getResource(Symbol.SLASH + qualifiedName.replace(Symbol.CONCAT, Symbol.SLASH) + Symbol.CONCAT + JavaKeywords.CLASS).getPath();
            String sourcePath = classpath.replace(Symbol.CONCAT + JavaKeywords.JAR, Symbol.HORIZONTAL_BAR + JavaKeywords.SOURCES + Symbol.CONCAT + JavaKeywords.JAR).replace(Symbol.CONCAT + JavaKeywords.CLASS, Symbol.CONCAT + JavaKeywords.JAVA);
            //file:/D:/maven/repository/org/hibernate/validator/hibernate-validator/6.0.18.Final/hibernate-validator-6.0.18.Final-sources.jar!/org/hibernate/validator/internal/metadata/provider/AnnotationMetaDataProvider.java
            try {
                //1.反编译
                URL url = new URL(JavaKeywords.JAR + Symbol.COLON + sourcePath);
                JarURLConnection connection = (JarURLConnection) url.openConnection();
                JarFile jarFile = connection.getJarFile();
                JarEntry jarEntry = connection.getJarEntry();
                String source;
                try (InputStream in = jarFile.getInputStream(jarEntry);
                     OutputStream out = new ByteArrayOutputStream()) {
                    IOUtils.copy(in, out);
                    source = out.toString();
                }
                //2.解析
                CompilationUnit compilationUnit = StaticJavaParser.parse(source);
                //3.构建代码
                NodeList<ImportDeclaration> imports = compilationUnit.getImports();
                this.imports.putAll(imports.stream()
                        .filter(f -> !f.isStatic())
                        .map(ImportDeclaration::getName)
                        .collect(Collectors.toMap(
                                Name::getIdentifier,
                                name -> name.getQualifier().map(Name::asString).orElse(Symbol.EMPTY))
                        )
                );
                targetPackageName = compilationUnit.getPackageDeclaration().map(NodeWithName::getNameAsString).orElse(null);
                ClassOrInterfaceDeclaration classOrInterface = compilationUnit.findFirst(ClassOrInterfaceDeclaration.class).orElse(null);
                if (Objects.nonNull(classOrInterface)) {
                    TypeSpec.Builder classBuilder = generateClassesOrInterfaceBuilder(classOrInterface, classname + IMPL);
                    classBuilder.addSuperinterface(TypeName.get(element.asType()));
                    JavaFile.Builder javaBuilder = JavaFile.builder(packageName, classBuilder.build());
                    imports.stream()
                            .filter(ImportDeclaration::isStatic)
                            .map(ImportDeclaration::getName)
                            .forEach(staticImport -> staticImport.getQualifier().ifPresent(qualifier -> javaBuilder.addStaticImport(ClassName.get(qualifier.getQualifier().map(Name::asString).orElse(Symbol.EMPTY), qualifier.getIdentifier()), staticImport.getIdentifier())));
                    javaBuilder.build().writeTo(filer);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return true;
    }

    /**
     * 生成类或接口生成器
     *
     * @param classOrInterface 类或接口
     * @param classname        类名称
     * @return {@code Builder}
     */
    private TypeSpec.Builder generateClassesOrInterfaceBuilder(ClassOrInterfaceDeclaration classOrInterface, String classname) {

        TypeSpec.Builder classOrInterfaceBuilder;
        if (classOrInterface.isInterface()) {
            classOrInterfaceBuilder = TypeSpec.interfaceBuilder(classname);
        } else {
            classOrInterfaceBuilder = TypeSpec.classBuilder(classname);
        }
        classOrInterfaceBuilder.addModifiers(getPublicModifiers(classOrInterface.getModifiers()));

        List<BodyDeclaration<?>> innerClassOrInterfaces = classOrInterface.getMembers().stream()
                .filter(BodyDeclaration::isClassOrInterfaceDeclaration)
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(innerClassOrInterfaces)) {
            innerClassOrInterfaces.forEach(e -> imports.put(((ClassOrInterfaceDeclaration) e).getNameAsString(), Symbol.EMPTY));
            for (BodyDeclaration<?> body : innerClassOrInterfaces) {
                ClassOrInterfaceDeclaration innerClassOrInterface = (ClassOrInterfaceDeclaration) body;
                String innerClassName = innerClassOrInterface.getNameAsString();
                TypeSpec.Builder innerClassOrInterfaceBuilder = generateClassesOrInterfaceBuilder(innerClassOrInterface, innerClassName);
                classOrInterfaceBuilder.addType(innerClassOrInterfaceBuilder.build());
            }
        }

        //泛型
        NodeList<TypeParameter> typeParameters = classOrInterface.getTypeParameters();
        if (CollectionUtils.isNotEmpty(typeParameters)) {
            this.typeParameters.addAll(typeParameters);
            classOrInterfaceBuilder.addTypeVariables(generateTypeVariableNames(typeParameters));
        }

        //继承类
        for (ClassOrInterfaceType extendedType : classOrInterface.getExtendedTypes()) {
            classOrInterfaceBuilder.superclass(generateGenericType(extendedType));
        }
        //实现接口
        List<TypeName> superinterfaces = generateSuperinterfaces(classOrInterface);
        classOrInterfaceBuilder.addSuperinterfaces(superinterfaces);

        //成员变量
        List<FieldSpec> fields = generateFields(classOrInterface);
        classOrInterfaceBuilder.addFields(fields);

        //初始化代码块
        for (InitializerDeclaration initializer : generateInitializers(classOrInterface)) {
            CodeBlock codeBlock = generateCodeBlock(initializer.getBody());
            if (initializer.isStatic()) {
                classOrInterfaceBuilder.addStaticBlock(codeBlock);
            } else {
                classOrInterfaceBuilder.addInitializerBlock(codeBlock);
            }
        }

        //构造函数
        List<MethodSpec> constructors = generateConstructors(classOrInterface);
        classOrInterfaceBuilder.addMethods(constructors);

        //方法
        List<MethodSpec> methods = generateMethods(classOrInterface);
        classOrInterfaceBuilder.addMethods(methods);
        return classOrInterfaceBuilder;
    }

    /**
     * 生成接口
     *
     * @param classOrInterface 类或接口
     * @return {@code List<TypeName>}
     */
    private List<TypeName> generateSuperinterfaces(ClassOrInterfaceDeclaration classOrInterface) {
        return classOrInterface.getImplementedTypes().stream().map(this::generateGenericType).collect(Collectors.toList());
    }

    /**
     * 生成字段
     *
     * @param classOrInterface 类或接口
     * @return {@code List<FieldSpec>}
     */
    private List<FieldSpec> generateFields(ClassOrInterfaceDeclaration classOrInterface) {
        return classOrInterface.getFields().stream()
                .map(field -> {
                    VariableDeclarator variable = field.getVariable(0);
                    String variableName = variable.getNameAsString();
                    TypeName variableType = generateGenericType(variable.getType());
                    Modifier[] modifiers = getPublicModifiers(field.getModifiers());

                    FieldSpec.Builder fieldBuilder = FieldSpec.builder(variableType, variableName, modifiers);
                    variable.getInitializer().ifPresent(initializer -> fieldBuilder.initializer(generateExpression(initializer)));
                    return fieldBuilder.build();
                }).collect(Collectors.toList());
    }

    /**
     * 生成初始化
     *
     * @param classOrInterface 类或接口
     * @return {@code List<InitializerDeclaration>}
     */
    private List<InitializerDeclaration> generateInitializers(ClassOrInterfaceDeclaration classOrInterface) {
        return classOrInterface.getMembers().stream()
                .filter(BodyDeclaration::isInitializerDeclaration)
                .map(BodyDeclaration::asInitializerDeclaration)
                .collect(Collectors.toList());
    }

    /**
     * 生成构造函数
     *
     * @param classOrInterface 类或接口
     * @return {@code List<MethodSpec>}
     */
    private List<MethodSpec> generateConstructors(ClassOrInterfaceDeclaration classOrInterface) {
        return classOrInterface.getConstructors().stream().map(constructor -> {
            List<ParameterSpec> parameters = generateParameters(constructor);

            MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)
                    .addParameters(parameters);

            for (Statement statement : constructor.getBody().getStatements()) {
                constructorBuilder.addCode(generateCodeBlock(statement));
            }
            return constructorBuilder.build();
        }).collect(Collectors.toList());
    }

    /**
     * 生成方法
     *
     * @param classOrInterface 类或接口
     * @return {@code List<MethodSpec>}
     */
    private List<MethodSpec> generateMethods(ClassOrInterfaceDeclaration classOrInterface) {
        return classOrInterface.getMethods().stream().map(method -> {
            NodeList<TypeParameter> typeParameters = method.getTypeParameters();
            if (CollectionUtils.isNotEmpty(typeParameters)) {
                this.typeParameters.addAll(typeParameters);
            }
            List<ParameterSpec> parameters = generateParameters(method);

            String methodName = method.getNameAsString();
            TypeName returnType = generateGenericType(method.getType());
            List<TypeVariableName> typeVariableNames = generateTypeVariableNames(typeParameters);
            MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder(methodName)
                    .addModifiers(Modifier.PUBLIC)
                    .addTypeVariables(typeVariableNames)
                    .returns(returnType)
                    .addParameters(parameters);
            if (classOrInterface.isInterface()) {
                methodBuilder.addModifiers(Modifier.ABSTRACT);
            }

            method.getBody().ifPresent(blockStmt -> {
                for (Statement statement : blockStmt.getStatements()) {
                    methodBuilder.addCode(generateCodeBlock(statement));
                }
            });
            return methodBuilder.build();
        }).collect(Collectors.toList());
    }

    /**
     * 生成方法泛型通配符
     *
     * @param typeParameters 泛型参数
     * @return {@code List<TypeVariableName>}
     */
    private List<TypeVariableName> generateTypeVariableNames(NodeList<TypeParameter> typeParameters) {
        return typeParameters.stream().map(typeParameter -> {
            String typeParameterName = typeParameter.getNameAsString();
            List<TypeName> typeParameterBounds = typeParameter.getTypeBound().stream().map(this::generateGenericType).collect(Collectors.toList());
            return TypeVariableName.get(typeParameterName).withBounds(typeParameterBounds);
        }).collect(Collectors.toList());
    }

    /**
     * 生成参数
     *
     * @param callable 可调用
     * @return {@code List<ParameterSpec>}
     */
    private List<ParameterSpec> generateParameters(CallableDeclaration<?> callable) {
        return callable.getParameters().stream().map(parameter -> {
            String parameterName = parameter.getNameAsString();
            TypeName parameterType = generateGenericType(parameter.getType());
            return ParameterSpec.builder(parameterType, parameterName).build();
        }).collect(Collectors.toList());
    }

    /**
     * 生成泛型类型
     *
     * @param type 类型
     * @return {@code TypeName}
     */
    private TypeName generateGenericType(Type type) {
        TypeName typeName = getTypeName(type);
        if (type.isClassOrInterfaceType()) {
            NodeList<Type> genericArguments = type.asClassOrInterfaceType().getTypeArguments().orElse(null);
            if (CollectionUtils.isNotEmpty(genericArguments)) {
                TypeName[] genericArgumentTypeNames = genericArguments.stream()
                        // BeanConfiguration<T>、BeanConfiguration<Object>
                        .map(this::generateGenericType).toArray(TypeName[]::new);
                // List<BeanConfiguration<Object>>
                return ParameterizedTypeName.get((ClassName) typeName, genericArgumentTypeNames);
            }
        }
        return typeName;
    }

    /**
     * 生成代码块
     *
     * @param statement 声明
     * @return {@code CodeBlock}
     */
    private CodeBlock generateCodeBlock(Statement statement) {
        StringBuilder builder = new StringBuilder();
        List<Object> args = new ArrayList<>();
        generateCodeBlock(statement, builder, args);
        return CodeBlock.of(builder.toString(), args.toArray());
    }

    /**
     * 生成代码块
     *
     * @param statement 声明
     * @param builder   构建器
     * @param args      参数
     */
    private void generateCodeBlock(Statement statement, StringBuilder builder, List<Object> args) {
        generateCodeBlock(statement, builder, args, false);
    }

    /**
     * 生成代码块
     *
     * @param statement    声明
     * @param builder      构建器
     * @param args         参数
     * @param isLambdaExpr 是否lambda表达式
     */
    private void generateCodeBlock(Statement statement, StringBuilder builder, List<Object> args, boolean isLambdaExpr) {
        if (statement.isReturnStmt()) {
            generateReturnStmt(statement.asReturnStmt(), builder, args);
            return;
        }
        if (statement.isExplicitConstructorInvocationStmt()) {
            generateExplicitConstructorInvocationStmt(statement.asExplicitConstructorInvocationStmt(), builder, args);
            return;
        }
        if (statement.isIfStmt()) {
            generateIfStmt(statement.asIfStmt(), builder, args);
            return;
        }
        if (statement.isExpressionStmt()) {
            generateExpressionStmt(statement.asExpressionStmt(), builder, args, isLambdaExpr);
            return;
        }
        if (statement.isBlockStmt()) {
            generateBlockStmt(statement.asBlockStmt(), builder, args);
            return;
        }
        if (statement.isForEachStmt()) {
            generateForEachStmt(statement.asForEachStmt(), builder, args);
            return;
        }
        if (statement.isForStmt()) {
            generateForStmt(statement.asForStmt(), builder, args);
            return;
        }
        if (statement.isWhileStmt()) {
            generateWhileStmt(statement.asWhileStmt(), builder, args);
            return;
        }
        if (statement.isTryStmt()) {
            generateTryStmt(statement.asTryStmt(), builder, args);
            return;
        }
        if (statement.isThrowStmt()) {
            generateThrowStmt(statement.asThrowStmt(), builder, args);
        }
    }

    /**
     * 生成显式构造函数调用
     *
     * @param explicitConstructorInvocationStmt 显式构造函数调用支撑
     * @param builder                           构建器
     * @param args                              参数
     */
    private void generateExplicitConstructorInvocationStmt(ExplicitConstructorInvocationStmt explicitConstructorInvocationStmt, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.SUPER).append(Symbol.LEFT_BRACKETS);
        generateArgumentsExpr(explicitConstructorInvocationStmt.getArguments(), builder, args);
        builder.append(Symbol.RIGHT_BRACKETS).append(Symbol.SEMICOLON).append(Symbol.NEWLINE);
    }

    /**
     * 生成try代码快
     *
     * @param tryStmt 试着支撑
     * @param builder 构建器
     * @param args    参数
     */
    private void generateTryStmt(TryStmt tryStmt, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.TRY)
                .append(Symbol.SPACE);
        generateBlockStmt(tryStmt.getTryBlock(), builder, args);
        NodeList<CatchClause> catchClauses = tryStmt.getCatchClauses();
        if (CollectionUtils.isEmpty(catchClauses)) {
            builder.append(Symbol.NEWLINE);
        } else {
            for (CatchClause catchClause : catchClauses) {
                builder.append(Symbol.SPACE)
                        .append(JavaKeywords.CATCH)
                        .append(Symbol.SPACE)
                        .append(Symbol.LEFT_BRACKETS);
                generateParameter(catchClause.getParameter(), builder, args);
                builder.append(Symbol.RIGHT_BRACKETS)
                        .append(Symbol.SPACE);
                generateBlockStmt(catchClause.getBody(), builder, args);
            }
        }
        BlockStmt finallyBlock = tryStmt.getFinallyBlock().orElse(null);
        if (Objects.isNull(finallyBlock)) {
            builder.append(Symbol.NEWLINE);
        } else {
            builder.append(Symbol.SPACE)
                    .append(JavaKeywords.FINALLY)
                    .append(Symbol.SPACE);
            generateBlockStmt(finallyBlock, builder, args);
        }
    }

    /**
     * 生成异常代码块
     *
     * @param throwStmt 把支撑
     * @param builder   构建器
     * @param args      参数
     */
    private void generateThrowStmt(ThrowStmt throwStmt, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.THROW)
                .append(Symbol.SPACE);
        generateExpression(throwStmt.getExpression(), builder, args);
        builder.append(Symbol.SEMICOLON).append(Symbol.NEWLINE);
    }

    /**
     * 生成foreach代码块
     *
     * @param forEachStmt 对于每一个支撑
     * @param builder     构建器
     * @param args        参数
     */
    private void generateForEachStmt(ForEachStmt forEachStmt, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.FOR).append(Symbol.SPACE).append(Symbol.LEFT_BRACKETS);
        generateExpression(forEachStmt.getVariable(), builder, args);
        builder.append(Symbol.SPACE).append(Symbol.COLON).append(Symbol.SPACE);
        generateExpression(forEachStmt.getIterable(), builder, args);
        builder.append(Symbol.RIGHT_BRACKETS).append(Symbol.SPACE);
        generateCodeBlock(forEachStmt.getBody(), builder, args);
        builder.append(Symbol.NEWLINE);
    }

    /**
     * 生成for代码块
     *
     * @param forStmt 对于每一个支撑
     * @param builder 构建器
     * @param args    参数
     */
    private void generateForStmt(ForStmt forStmt, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.FOR).append(Symbol.SPACE).append(Symbol.LEFT_BRACKETS);
        generateArgumentsExpr(forStmt.getInitialization(), builder, args);
        builder.append(Symbol.SEMICOLON).append(Symbol.SPACE);
        forStmt.getCompare().ifPresent(compare -> generateExpression(compare, builder, args));
        builder.append(Symbol.SEMICOLON).append(Symbol.SPACE);
        generateArgumentsExpr(forStmt.getUpdate(), builder, args);
        builder.append(Symbol.RIGHT_BRACKETS).append(Symbol.SPACE);
        generateCodeBlock(forStmt.getBody(), builder, args);
        builder.append(Symbol.NEWLINE);
    }

    /**
     * 生成while支撑
     *
     * @param whileStmt while支撑
     * @param builder   构建器
     * @param args      参数
     */
    private void generateWhileStmt(WhileStmt whileStmt, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.WHILE).append(Symbol.LEFT_BRACKETS);
        generateExpression(whileStmt.getCondition(), builder, args);
        builder.append(Symbol.RIGHT_BRACKETS).append(Symbol.SPACE);
        generateCodeBlock(whileStmt.getBody(), builder, args);
        builder.append(Symbol.NEWLINE);
    }

    /**
     * 生成块代码
     *
     * @param blockStmt 块支撑
     * @param builder   构建器
     * @param args      参数
     */
    private void generateBlockStmt(BlockStmt blockStmt, StringBuilder builder, List<Object> args) {
        builder.append(Symbol.LEFT_CURLY_BRACKETS)
                .append(Symbol.NEWLINE)
                .append(Symbol.TAB);
        NodeList<Statement> statements = blockStmt.getStatements();
        if (CollectionUtils.isNotEmpty(statements)) {
            int size = statements.size();
            for (int i = 0; i < statements.size(); i++) {
                generateCodeBlock(statements.get(i), builder, args);
                if (i < size - 1) {
                    builder.append(Symbol.TAB);
                }
            }
        }
        builder.append(Symbol.RIGHT_CURLY_BRACKETS);
    }

    /**
     * 生成表达式代码
     *
     * @param expressionStmt 表达支撑
     * @param builder        构建器
     * @param args           参数
     * @param isLambdaExpr   是否lambda表达式
     */
    private void generateExpressionStmt(ExpressionStmt expressionStmt, StringBuilder builder, List<Object> args, boolean isLambdaExpr) {
        generateExpression(expressionStmt.getExpression(), builder, args);
        if (!isLambdaExpr) {
            builder.append(Symbol.SEMICOLON).append(Symbol.NEWLINE);
        }
    }

    /**
     * 生成if代码
     *
     * @param ifStmt  如果支撑
     * @param builder 构建器
     * @param args    参数
     */
    private void generateIfStmt(IfStmt ifStmt, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.IF)
                .append(Symbol.SPACE)
                .append(Symbol.LEFT_BRACKETS);
        generateExpression(ifStmt.getCondition(), builder, args);
        builder.append(Symbol.RIGHT_BRACKETS)
                .append(Symbol.SPACE);
        generateCodeBlock(ifStmt.getThenStmt(), builder, args);
        Statement elseStmt = ifStmt.getElseStmt().orElse(null);
        if (Objects.isNull(elseStmt)) {
            builder.append(Symbol.NEWLINE);
        } else {
            builder.append(Symbol.SPACE)
                    .append(JavaKeywords.ELSE)
                    .append(Symbol.SPACE);
            generateCodeBlock(elseStmt, builder, args);
        }
    }

    /**
     * 生成return代码
     *
     * @param returnStmt 返回支撑
     * @param builder    构建器
     * @param args       参数
     */
    private void generateReturnStmt(ReturnStmt returnStmt, StringBuilder builder, List<Object> args) {
        returnStmt.getExpression()
                .ifPresent(expression -> {
                    builder.append(JavaKeywords.RETURN)
                            .append(Symbol.SPACE);
                    generateExpression(expression, builder, args);
                });
        builder.append(Symbol.SEMICOLON).append(Symbol.NEWLINE);
    }

    /**
     * 生成表达式
     *
     * @param expression 表达式
     * @return {@code CodeBlock}
     */
    private CodeBlock generateExpression(Expression expression) {
        StringBuilder builder = new StringBuilder();
        List<Object> args = new ArrayList<>();
        generateExpression(expression, builder, args);
        return CodeBlock.of(builder.toString(), args.toArray());
    }

    /**
     * 生成表达式
     *
     * @param expression 表达式
     * @param builder    构建器
     * @param args       arg参数
     */
    private void generateExpression(Expression expression, StringBuilder builder, List<Object> args) {
        if (expression.isMethodCallExpr()) {
            generateMethodCallExpr(expression.asMethodCallExpr(), builder, args);
            return;
        }
        if (expression.isMethodReferenceExpr()) {
            generateMethodReferenceExpr(expression.asMethodReferenceExpr(), builder, args);
            return;
        }
        if (expression.isLambdaExpr()) {
            generateLambdaExpr(expression.asLambdaExpr(), builder, args);
            return;
        }
        if (expression.isTypeExpr()) {
            generateTypeExpr(expression.asTypeExpr(), builder, args);
            return;
        }
        if (expression.isClassExpr()) {
            generateClassExpr(expression.asClassExpr(), builder, args);
            return;
        }
        if (expression.isArrayCreationExpr()) {
            generateArrayCreationExpr(expression.asArrayCreationExpr(), builder, args);
            return;
        }
        if (expression.isObjectCreationExpr()) {
            generateObjectCreationExpr(expression.asObjectCreationExpr(), builder, args);
            return;
        }
        if (expression.isAssignExpr()) {
            generateAssignExpr(expression.asAssignExpr(), builder, args);
            return;
        }
        if (expression.isFieldAccessExpr()) {
            generateFieldAccessExpr(expression.asFieldAccessExpr(), builder, args);
            return;
        }
        if (expression.isArrayAccessExpr()) {
            generateArrayAccessExpr(expression.asArrayAccessExpr(), builder, args);
            return;
        }
        if (expression.isThisExpr()) {
            generateThisExpr(expression.asThisExpr(), builder, args);
        }
        if (expression.isNameExpr()) {
            generateNameExpr(expression.asNameExpr(), builder, args);
            return;
        }
        if (expression.isVariableDeclarationExpr()) {
            generateVariableDeclarationExpr(expression.asVariableDeclarationExpr(), builder, args);
            return;
        }
        if (expression.isCastExpr()) {
            generateCastExpr(expression.asCastExpr(), builder, args);
            return;
        }
        if (expression.isUnaryExpr()) {
            generateUnaryExpr(expression.asUnaryExpr(), builder, args);
            return;
        }
        if (expression.isConditionalExpr()) {
            generateConditionalExpr(expression.asConditionalExpr(), builder, args);
            return;
        }
        if (expression.isInstanceOfExpr()) {
            generateInstanceOfExpr(expression.asInstanceOfExpr(), builder, args);
            return;
        }
        if (expression.isEnclosedExpr()) {
            generateEnclosedExpr(expression.asEnclosedExpr(), builder, args);
            return;
        }
        if (expression.isNullLiteralExpr()) {
            generateNullLiteralExpr(expression.asNullLiteralExpr(), builder, args);
            return;
        }
        if (expression.isStringLiteralExpr()) {
            generateStringLiteralExpr(expression.asStringLiteralExpr(), builder, args);
            return;
        }
        if (expression.isIntegerLiteralExpr()) {
            generateIntegerLiteralExpr(expression.asIntegerLiteralExpr(), builder, args);
            return;
        }
        if (expression.isBinaryExpr()) {
            generateBinaryExpr(expression.asBinaryExpr(), builder, args);
        }
    }

    /**
     * 生成 instanceof expr
     *
     * @param instanceOfExpr instanceof expr
     * @param builder        构建器
     * @param args           参数
     */
    private void generateInstanceOfExpr(InstanceOfExpr instanceOfExpr, StringBuilder builder, List<Object> args) {
        generateExpression(instanceOfExpr.getExpression(), builder, args);
        builder.append(Symbol.SPACE)
                .append(JavaKeywords.INSTANCEOF)
                .append(Symbol.SPACE)
                .append($T);
        args.add(generateGenericType(instanceOfExpr.getType()));
    }

    /**
     * 生成参数
     *
     * @param parameter 参数
     * @param builder   构建器
     * @param args      参数
     */
    private void generateParameter(Parameter parameter, StringBuilder builder, List<Object> args) {
        Type parameterType = parameter.getType();
        if (!parameterType.isUnknownType()) {
            builder.append($T)
                    .append(Symbol.SPACE);
            args.add(generateGenericType(parameterType));
        }
        builder.append(parameter.getNameAsString());
    }

    /**
     * 生成lambda表达式
     *
     * @param lambdaExpr λexpr
     * @param builder    构建器
     * @param args       参数
     */
    private void generateLambdaExpr(LambdaExpr lambdaExpr, StringBuilder builder, List<Object> args) {
        NodeList<Parameter> parameters = lambdaExpr.getParameters();
        builder.append(Symbol.LEFT_BRACKETS);
        if (CollectionUtils.isNotEmpty(parameters)) {
            int size = parameters.size();
            for (int i = 0; i < size; i++) {
                generateParameter(parameters.get(i), builder, args);
                if (i < size - 1) {
                    builder.append(Symbol.COMMA_SPACE);
                }
            }
        }
        builder.append(Symbol.RIGHT_BRACKETS)
                .append(Symbol.SPACE)
                .append(Symbol.RIGHT_ARROW)
                .append(Symbol.SPACE);
        generateCodeBlock(lambdaExpr.getBody(), builder, args, true);
    }

    /**
     * 生成类型expr
     *
     * @param typeExpr 类型expr
     * @param builder  构建器
     * @param args     参数
     */
    private void generateTypeExpr(TypeExpr typeExpr, StringBuilder builder, List<Object> args) {
        builder.append($T);
        args.add(generateGenericType(typeExpr.getType()));
    }

    /**
     * 生成方法引用expr
     *
     * @param methodReferenceExpr 方法引用expr
     * @param builder             构建器
     * @param args                参数
     */
    private void generateMethodReferenceExpr(MethodReferenceExpr methodReferenceExpr, StringBuilder builder, List<Object> args) {
        generateExpression(methodReferenceExpr.getScope(), builder, args);
        builder.append(Symbol.SCOPE_OPERATOR).append(methodReferenceExpr.getIdentifier());
    }

    /**
     * 生成数组访问 expr
     *
     * @param arrayAccessExpr 数组访问expr
     * @param builder         构建器
     * @param args            参数
     */
    private void generateArrayAccessExpr(ArrayAccessExpr arrayAccessExpr, StringBuilder builder, List<Object> args) {
        generateExpression(arrayAccessExpr.getName(), builder, args);
        builder.append(Symbol.LEFT_SQUARE_BRACKETS)
                .append(arrayAccessExpr.getIndex())
                .append(Symbol.RIGHT_SQUARE_BRACKETS);
    }

    /**
     * 生成数字 expr
     *
     * @param integerLiteralExpr 整数字面expr
     * @param builder            构建器
     * @param args               参数
     */
    private void generateIntegerLiteralExpr(IntegerLiteralExpr integerLiteralExpr, StringBuilder builder, List<Object> args) {
        builder.append(integerLiteralExpr.getValue());
    }

    /**
     * 生成字符串 expr
     *
     * @param stringLiteralExpr 字符串expr
     * @param builder           构建器
     * @param args              参数
     */
    private void generateStringLiteralExpr(StringLiteralExpr stringLiteralExpr, StringBuilder builder, List<Object> args) {
        builder.append($S);
        args.add(stringLiteralExpr.getValue());
    }

    /**
     * 生成 != expr
     *
     * @param binaryExpr 二进制expr
     * @param builder    构建器
     * @param args       参数
     */
    private void generateBinaryExpr(BinaryExpr binaryExpr, StringBuilder builder, List<Object> args) {
        generateExpression(binaryExpr.getLeft(), builder, args);
        builder.append(Symbol.SPACE)
                .append(binaryExpr.getOperator().asString())
                .append(Symbol.SPACE);
        generateExpression(binaryExpr.getRight(), builder, args);
    }

    /**
     * 生成null expr
     *
     * @param nullLiteralExpr 零文字expr
     * @param builder         构建器
     * @param args            参数
     */
    private void generateNullLiteralExpr(NullLiteralExpr nullLiteralExpr, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.NULL);
    }

    /**
     * 生成封闭expr
     *
     * @param enclosedExpr 封闭expr
     * @param builder      构建器
     * @param args         参数
     */
    private void generateEnclosedExpr(EnclosedExpr enclosedExpr, StringBuilder builder, List<Object> args) {
        builder.append(Symbol.LEFT_BRACKETS);
        generateExpression(enclosedExpr.getInner(), builder, args);
        builder.append(Symbol.RIGHT_BRACKETS);
    }

    /**
     * 生成三元表达式 expr
     *
     * @param conditionalExpr 条件expr
     * @param builder         构建器
     * @param args            参数
     */
    private void generateConditionalExpr(ConditionalExpr conditionalExpr, StringBuilder builder, List<Object> args) {
        generateExpression(conditionalExpr.getCondition(), builder, args);
        builder.append(Symbol.SPACE)
                .append(Symbol.QUESTION)
                .append(Symbol.SPACE);
        generateExpression(conditionalExpr.getThenExpr(), builder, args);
        builder.append(Symbol.SPACE)
                .append(Symbol.COLON)
                .append(Symbol.SPACE);
        generateExpression(conditionalExpr.getElseExpr(), builder, args);
    }

    /**
     * 产生一元expr
     *
     * @param unaryExpr 一元expr
     * @param builder   构建器
     * @param args      参数
     */
    private void generateUnaryExpr(UnaryExpr unaryExpr, StringBuilder builder, List<Object> args) {
        UnaryExpr.Operator operator = unaryExpr.getOperator();
        if (operator.isPrefix()) {
            builder.append(operator.asString());
        }
        generateExpression(unaryExpr.getExpression(), builder, args);
        if (operator.isPostfix()) {
            builder.append(operator.asString());
        }
    }

    /**
     * 生成转换expr
     *
     * @param castExpr 演员expr
     * @param builder  构建器
     * @param args     参数
     */
    private void generateCastExpr(CastExpr castExpr, StringBuilder builder, List<Object> args) {
        builder.append(Symbol.LEFT_BRACKETS)
                .append($T)
                .append(Symbol.RIGHT_BRACKETS)
                .append(Symbol.SPACE);
        args.add(generateGenericType(castExpr.getType()));
        generateExpression(castExpr.getExpression(), builder, args);
    }

    /**
     * 生成变量声明expr
     *
     * @param variableDeclarationExpr 变量声明expr
     * @param builder                 构建器
     * @param args                    参数
     */
    private void generateVariableDeclarationExpr(VariableDeclarationExpr variableDeclarationExpr, StringBuilder builder, List<Object> args) {
        NodeList<VariableDeclarator> variables = variableDeclarationExpr.getVariables();
        if (CollectionUtils.isNotEmpty(variables)) {
            int size = variables.size();
            for (int i = 0; i < variables.size(); i++) {
                VariableDeclarator variable = variables.get(i);
                if (i == 0) {
                    builder.append($T)
                            .append(Symbol.SPACE);
                    args.add(generateGenericType(variable.getType()));
                }
                builder.append(variable.getNameAsString());
                if (i < size - 1) {
                    builder.append(Symbol.COMMA_SPACE);
                }
                variable.getInitializer().ifPresent(initializer -> {
                    builder.append(Symbol.SPACE)
                            .append(Symbol.EQUAL)
                            .append(Symbol.SPACE);
                    generateExpression(initializer, builder, args);
                });
            }
        }
    }

    /**
     * 生成this expr
     *
     * @param thisExpr 这expr
     * @param builder  构建器
     * @param args     参数
     */
    private void generateThisExpr(ThisExpr thisExpr, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.THIS);
    }

    /**
     * 生成名字expr
     *
     * @param nameExpr 名字expr
     * @param builder  构建器
     * @param args     参数
     */
    private void generateNameExpr(NameExpr nameExpr, StringBuilder builder, List<Object> args) {
        String scopeName = nameExpr.getNameAsString();
        if (imports.containsKey(scopeName)) {
            //静态方法
            builder.append($T);
            args.add(ClassName.get(imports.getOrDefault(scopeName, targetPackageName), scopeName));
        } else {
            //实例方法
            builder.append(scopeName);
        }
    }

    /**
     * 生成class expr
     *
     * @param classExpr 类expr
     * @param builder   构建器
     * @param args      参数
     */
    private void generateClassExpr(ClassExpr classExpr, StringBuilder builder, List<Object> args) {
        builder.append($T).append(Symbol.CONCAT).append(JavaKeywords.CLASS);
        args.add(generateGenericType(classExpr.getType()));
    }

    /**
     * 生成字段expr
     *
     * @param fieldAccessExpr 字段访问expr
     * @param builder         构建器
     * @param args            参数
     */
    private void generateFieldAccessExpr(FieldAccessExpr fieldAccessExpr, StringBuilder builder, List<Object> args) {
        generateExpression(fieldAccessExpr.getScope(), builder, args);
        builder.append(Symbol.CONCAT).append(fieldAccessExpr.getNameAsString());
    }

    /**
     * 生成赋值expr
     *
     * @param assignExpr 分配expr
     * @param builder    构建器
     * @param args       参数
     */
    private void generateAssignExpr(AssignExpr assignExpr, StringBuilder builder, List<Object> args) {
        generateExpression(assignExpr.getTarget(), builder, args);
        builder.append(Symbol.SPACE)
                .append(assignExpr.getOperator().asString())
                .append(Symbol.SPACE);
        generateExpression(assignExpr.getValue(), builder, args);
    }

    /**
     * 生成对象创建expr
     *
     * @param objectCreationExpr 对象创建expr
     * @param builder            构建器
     * @param args               参数
     */
    private void generateObjectCreationExpr(ObjectCreationExpr objectCreationExpr, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.NEW)
                .append(Symbol.SPACE)
                .append($T)
                .append(Symbol.LEFT_BRACKETS);
        args.add(generateGenericType(objectCreationExpr.getType()));
        generateArgumentsExpr(objectCreationExpr.getArguments(), builder, args);
        builder.append(Symbol.RIGHT_BRACKETS);
    }

    /**
     * 生成数组创建expr
     *
     * @param arrayCreationExpr 数组创建expr
     * @param builder           构建器
     * @param args              参数
     */
    private void generateArrayCreationExpr(ArrayCreationExpr arrayCreationExpr, StringBuilder builder, List<Object> args) {
        builder.append(JavaKeywords.NEW)
                .append(Symbol.SPACE)
                .append($T)
                .append(Symbol.LEFT_SQUARE_BRACKETS);
        NodeList<ArrayCreationLevel> levels = arrayCreationExpr.getLevels();
        if (CollectionUtils.isNotEmpty(levels)) {
            int size = levels.size();
            for (int i = 0; i < levels.size(); i++) {
                levels.get(i).getDimension().ifPresent(dimension -> generateExpression(dimension, builder, args));
                if (i < size - 1) {
                    builder.append(Symbol.COMMA_SPACE);
                }
            }
        }
        builder.append(Symbol.RIGHT_SQUARE_BRACKETS);
        args.add(generateGenericType(arrayCreationExpr.getElementType()));
        arrayCreationExpr.getInitializer().ifPresent(arrayInitializerExpr -> {
            builder.append(Symbol.LEFT_CURLY_BRACKETS)
                    .append(Symbol.SPACE);
            generateArgumentsExpr(arrayInitializerExpr.getValues(), builder, args);
            builder.append(Symbol.SPACE)
                    .append(Symbol.RIGHT_CURLY_BRACKETS);
        });
    }

    /**
     * 生成方法调用expr
     *
     * @param methodCallExpr 方法调用expr
     * @param builder        构建器
     * @param args           参数
     */
    private void generateMethodCallExpr(MethodCallExpr methodCallExpr, StringBuilder builder, List<Object> args) {
        Expression scope = methodCallExpr.getScope().orElse(null);
        if (Objects.nonNull(scope)) {
            generateExpression(scope, builder, args);
            builder.append(Symbol.CONCAT);
        }
        methodCallExpr.getTypeArguments().ifPresent(typeArguments -> generateMethodTypeArguments(typeArguments, builder, args));
        builder.append(methodCallExpr.getNameAsString())
                .append(Symbol.LEFT_BRACKETS);
        generateArgumentsExpr(methodCallExpr.getArguments(), builder, args);
        builder.append(Symbol.RIGHT_BRACKETS);
    }

    /**
     * 生成方法泛型参数
     *
     * @param typeArguments 泛型参数
     * @param builder       构建器
     * @param args          参数
     */
    private void generateMethodTypeArguments(NodeList<Type> typeArguments, StringBuilder builder, List<Object> args) {
        if (CollectionUtils.isNotEmpty(typeArguments)) {
            builder.append(Symbol.LEFT_ANGLE_BRACKETS);
            int size = typeArguments.size();
            for (int i = 0; i < typeArguments.size(); i++) {
                builder.append($T);
                if (i < size - 1) {
                    builder.append(Symbol.COMMA_SPACE);
                }
                args.add(generateGenericType(typeArguments.get(i)));
            }
            builder.append(Symbol.RIGHT_ANGLE_BRACKETS);
        }
    }

    /**
     * 生成参数
     *
     * @param arguments 参数
     * @param builder   构建器
     * @param args      参数
     */
    private void generateArgumentsExpr(NodeList<Expression> arguments, StringBuilder builder, List<Object> args) {
        if (CollectionUtils.isNotEmpty(arguments)) {
            int size = arguments.size();
            for (int i = 0; i < size; i++) {
                generateExpression(arguments.get(i), builder, args);
                if (i < size - 1) {
                    builder.append(Symbol.COMMA_SPACE);
                }
            }
        }
    }

    /**
     * 获取TypeName
     *
     * @param type 类型
     * @return {@code String}
     */
    private TypeName getTypeName(Type type) {
        String typeName;
        List<String> typeParameterNames = CollUtils.extractProperties(this.typeParameters, NodeWithSimpleName::getNameAsString);
        if (type.isArrayType()) {
            TypeName componentTypeName = generateGenericType(type.asArrayType().getComponentType());
            return ArrayTypeName.of(componentTypeName);
        } else if (type.isClassOrInterfaceType()) {
            ClassOrInterfaceType classOrInterfaceType = type.asClassOrInterfaceType();
            typeName = classOrInterfaceType.getNameAsString();
            if (typeParameterNames.contains(typeName)) {
                return TypeVariableName.get(typeName);
            }
            ClassOrInterfaceType scope = classOrInterfaceType.getScope().orElse(null);
            if (Objects.nonNull(scope)) {
                String scopeName = scope.toString();
                //java.lang.reflect
                if (scopeName.contains(Symbol.CONCAT)) {
                    return ClassName.get(scopeName, typeName);
                }
                return ((ClassName) getTypeName(scope)).nestedClass(typeName);
            }
            return ClassName.get(imports.getOrDefault(typeName, targetPackageName), typeName);
        } else if (type.isPrimitiveType()) {
            typeName = type.asPrimitiveType().asString();
            switch (typeName) {
                case JavaKeywords.BYTE:
                    return TypeName.BYTE;
                case JavaKeywords.SHORT:
                    return TypeName.SHORT;
                case JavaKeywords.INT:
                    return TypeName.INT;
                case JavaKeywords.LONG:
                    return TypeName.LONG;
                case JavaKeywords.FLOAT:
                    return TypeName.FLOAT;
                case JavaKeywords.DOUBLE:
                    return TypeName.DOUBLE;
                case JavaKeywords.CHAR:
                    return TypeName.CHAR;
                case JavaKeywords.BOOLEAN:
                    return TypeName.BOOLEAN;
                default:
                    return TypeName.VOID;
            }
        } else if (type.isVoidType()) {
            return TypeName.VOID;
        } else if (type.isWildcardType()) {
            return TypeVariableName.get(type.asWildcardType().asString());
        }
        return TypeName.OBJECT;
    }

    /**
     * 获得公众修饰符
     *
     * @param modifiers 修饰符
     * @return {@code Modifier[]}
     */
    private Modifier[] getPublicModifiers(NodeList<com.github.javaparser.ast.Modifier> modifiers) {
        if (CollectionUtils.isEmpty(modifiers)) {
            return new Modifier[]{Modifier.PUBLIC};
        }
        return modifiers.stream()
                .peek(modifier -> {
                    if (Arrays.asList(com.github.javaparser.ast.Modifier.Keyword.DEFAULT, com.github.javaparser.ast.Modifier.Keyword.PRIVATE, com.github.javaparser.ast.Modifier.Keyword.PROTECTED).contains(modifier.getKeyword())) {
                        modifier.setKeyword(com.github.javaparser.ast.Modifier.Keyword.PUBLIC);
                    }
                })
                .map(modifier -> Modifier.valueOf(modifier.getKeyword().asString().toUpperCase()))
                .toArray(Modifier[]::new);
    }
}
