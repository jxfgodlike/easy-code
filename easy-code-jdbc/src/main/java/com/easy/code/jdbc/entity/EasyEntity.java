package com.easy.code.jdbc.entity;

import com.easy.code.core.consts.YesOrNo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 简单实体
 *
 * @author jxfgodlike
 * @date 2022-06-18 0:32
 */
@Data
@Accessors(chain = true)
public class EasyEntity {

    /**
     * 是否删除
     */
    private Integer isDeleted;
    /**
     * 创建人
     */
    private Long createdBy;
    /**
     * 创建时间
     */
    private LocalDateTime createdDate;
    /**
     * 修改人
     */
    private Long updatedBy;
    /**
     * 修改时间
     */
    private LocalDateTime updatedDate;

    /**
     * 填充创建
     *
     * @param createBy 创建
     */
    public void fillCreateBy(Long createBy) {
        LocalDateTime now = LocalDateTime.now();
        this.setIsDeleted(YesOrNo.NO).setCreatedBy(createBy).setCreatedDate(now).setUpdatedBy(createBy).setUpdatedDate(now);
    }

    /**
     * 填充更新
     *
     * @param updateBy 更新
     */
    public void fillUpdateBy(Long updateBy) {
        LocalDateTime now = LocalDateTime.now();
        this.setUpdatedBy(updateBy).setUpdatedDate(now);
    }
}
