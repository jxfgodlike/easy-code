package com.easy.code.jdbc.entity;

import com.easy.code.jdbc.listener.JpaEntityAuditingListener;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * jpa实体
 *
 * @author jxfgodlike
 * @date 2022-06-18 0:32
 */
@Data
@Accessors(chain = true)
@MappedSuperclass
@EntityListeners({JpaEntityAuditingListener.class})
public class JpaEntity extends EasyEntity {

    /**
     * 是否删除
     */
    @Column(name = "is_deleted", updatable = false)
    private Integer isDeleted;
    /**
     * 创建人
     */
    @Column(name = "created_by", updatable = false)
    private Long createdBy;
    /**
     * 创建时间
     */
    @Column(name = "created_date", updatable = false)
    private LocalDateTime createdDate;
    /**
     * 修改人
     */
    private Long updatedBy;
    /**
     * 修改时间
     */
    private LocalDateTime updatedDate;
}
