##导入宏定义
$!{define.vm}

#set($pkg = "service")
#set($suffix = "Service")

##拿到主键
#set($pk = $tableInfo.pkColumn.get(0))

##定义实体对象名
#set($entityType = $!tableInfo.name + "Entity")
#set($entityName = $!tool.firstLowerCase($!tableInfo.name))

#set($viewType = $!tableInfo.name + "View")
#set($viewName = $!tool.firstLowerCase($!tableInfo.name) + "Req")

##设置表后缀（宏定义）
#setTableSuffix($!{suffix})

##保存文件（宏定义）
#save("/$!{pkg.replace('.', '/')}", "$!{suffix}.java")

##包路径（宏定义）
#setPackageSuffix($!{pkg})

##表注释（宏定义）
#tableComment("表服务接口")
public interface $!{tableName} {

    /**
     * 根据id查询
     *
     * @param $!pk.name 主键id
     * @return {@link $!entityType}
     */
    $!entityType findById($!pk.shortType $!pk.name);
    /**
     * 查询单条
     *
     * @param $!entityName 实体对象
     * @return {@link $!entityType}
     */
    $!entityType findOne($!entityType $!entityName);
    /**
     * 查询列表
     *
     * @param $!entityName 实体对象
     * @return {@link List}<{@link $!entityType}>
     */
    List<$!entityType> findAll($!entityType $!entityName);
    /**
     * 保存
     *
     * @param $!entityName 实体对象
     * @return {@link $!entityType}
     */
    $!entityType save($!entityType $!entityName);
    /**
     * 根据id修改
     *
     * @param $!{entityName} 实体对象
     * @return {@link $!entityType}
     */
    $!entityType updateById($!entityType $!{entityName});
    /**
     * 根据id删除
     *
     * @param $!pk.name 主键id
     */
    void deleteById($!pk.shortType $!pk.name);
    /**
     * 批量保存（MyBatis原生foreach方法）
     *
     * @param $!{entityName}s 实体对象列表
     * @return {@link List}<{@link $!entityType}>
     */
    List<$!entityType> saveBatch(List<$!entityType> $!{entityName}s);
    /**
     * 根据id批量修改（MyBatis原生foreach方法）
     *
     * @param $!{entityName}s 实体对象列表
     * @return {@link List}<{@link $!entityType}>
     */
    List<$!entityType> updateBatchById(List<$!entityType> $!{entityName}s);
    /**
     * 根据id批量删除（MyBatis原生foreach方法）
     *
     * @param $!{pk.name}s 主键ids
     */
    void deleteBatchByIds(List<$!pk.shortType> $!{pk.name}s);
    /**
     * 查询业务列表
     *
     * @param $!viewName 请求参数
     * @return {@link List}<{@link $!viewType}>
     */
    List<$!viewType> get$!{tableInfo.name}s($!viewType $!viewName);
    /**
     * 保存业务
     *
     * @param $!viewName 请求参数
     * @return {@link $!viewType}
     */
    $!viewType save$!{tableInfo.name}($!viewType $!viewName);
    /**
     * 根据id删除业务
     *
     * @param $!pk.name 主键id
     * @return {@link $!viewType}
     */
    $!viewType delete$!{tableInfo.name}ById($!pk.shortType $!pk.name);
    /**
     * 根据id修改业务
     *
     * @param $!viewName 请求参数
     * @return {@link $!viewType}
     */
    $!viewType update$!{tableInfo.name}ById($!viewType $!viewName);
    /**
     * 根据id查询业务
     *
     * @param $!pk.name 主键id
     * @return {@code $!viewType}
     */
    $!viewType get$!{tableInfo.name}ById($!pk.shortType $!pk.name);
}