package com.easy.code.jdbc.listener;

import com.easy.code.jdbc.entity.JpaEntity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * jpa实体审计侦听器
 *
 * @author jxfgodlike
 * @date 2022-07-09 22:55
 */
public class JpaEntityAuditingListener {

    /**
     * 填充创建
     *
     * @param entity 实体
     */
    @PrePersist
    public void fillCreateBy(JpaEntity entity) {
        entity.fillCreateBy(null);
    }

    /**
     * 填充更新
     *
     * @param entity 实体
     */
    @PreUpdate
    public void fillUpdateBy(JpaEntity entity) {
        entity.fillUpdateBy(null);
    }
}
