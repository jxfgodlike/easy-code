package com.easy.code.validation.annotation;

import com.easy.code.validation.config.EasyValidationConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用easy-validation
 *
 * @author jxfgodlike
 * @date 2022-04-19 23:45
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(EasyValidationConfig.class)
public @interface EnableEasyValidation {
}
