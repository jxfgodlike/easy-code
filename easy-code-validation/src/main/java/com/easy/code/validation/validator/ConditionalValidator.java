package com.easy.code.validation.validator;

import com.easy.code.validation.constraints.Conditional;
import com.easy.code.validation.metadata.impl.EasyConstraintValidatorContextImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * 条件验证器
 *
 * @author jxfgodlike
 * @date 2022-04-15 21:47
 */
public class ConditionalValidator implements ConstraintValidator<Conditional, Object> {

    private static final String ROOT = "root";

    private String expression;

    @Autowired
    private SpelExpressionParser spelExpressionParser;

    @Override
    public void initialize(Conditional constraintAnnotation) {
        expression = constraintAnnotation.expression();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
        Object target = ((EasyConstraintValidatorContextImpl) constraintValidatorContext).getTarget();
        EvaluationContext evaluationContext = new StandardEvaluationContext();
        evaluationContext.setVariable(ROOT, target);
        return Objects.equals(spelExpressionParser.parseExpression(expression).getValue(evaluationContext, Boolean.class), true);
    }
}
