package com.easy.code.core.config;

import com.easy.code.core.exception.GlobalExceptionHandler;
import com.easy.code.core.utils.SpringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * easy-core配置
 *
 * @author jxfgodlike
 * @date 2022-04-23 21:14
 */
@Configuration
@Import({SpringUtils.class, GlobalExceptionHandler.class})
public class EasyCoreConfig {

}
