package com.easy.code.core.exception;

import com.easy.code.core.enums.HttpStatusEnum;

/**
 * 参数异常
 *
 * @author jxfgodlike
 * @date 2022-05-31 21:46
 */
public class ParameterException extends EasyException {
    public ParameterException(String message) {
        super(HttpStatusEnum.BAD_REQUEST.code(), message);
    }
}
