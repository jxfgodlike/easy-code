package com.easy.code.validation.metadata;

import com.easy.code.processor.annotation.CopyOf;

/**
 * 简单约束树
 *
 * @author jxfgodlike
 * @date 2022-06-01 21:37
 */
@CopyOf("org.hibernate.validator.internal.engine.constraintvalidation.SimpleConstraintTree")
public interface SimpleConstraintTree {
}
