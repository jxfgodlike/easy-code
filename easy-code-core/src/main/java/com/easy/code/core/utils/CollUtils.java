package com.easy.code.core.utils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 集合工具
 *
 * @author jxfgodlike
 * @date 2022-05-11 20:39
 */
public class CollUtils {

    /**
     * 提取属性
     *
     * @param collection 集合
     * @param extractor  提取器
     * @return {@link List}<{@link K}>
     */
    public static <K, V> List<K> extractProperties(Collection<V> collection, Function<V, K> extractor) {
        return collection.stream().map(extractor).collect(Collectors.toList());
    }

    /**
     * 提取属性Set
     *
     * @param collection 集合
     * @param extractor  提取器
     * @return {@link Set}<{@link K}>
     */
    public static <K, V> Set<K> extractPropertySet(Collection<V> collection, Function<V, K> extractor) {
        return collection.stream().map(extractor).collect(Collectors.toSet());
    }

    /**
     * list转map
     *
     * @param collection 集合
     * @param extractor  提取器
     * @return {@code Map<K, V>}
     */
    public static <K, V> Map<K, V> toMap(Collection<V> collection, Function<V, K> extractor) {
        return collection.stream().collect(Collectors.toMap(extractor, Function.identity(), (a, b) -> a));
    }

    /**
     * 分组
     *
     * @param collection 集合
     * @param extractor  提取器
     * @return {@code Map<K, List<V>>}
     */
    public static <K, V> Map<K, List<V>> groupByKey(Collection<V> collection, Function<V, K> extractor) {
        return collection.stream().collect(Collectors.groupingBy(extractor));
    }
}
