##导入宏定义
$!{define.vm}

#set($pkg = "service.impl")
#set($suffix = "ServiceImpl")

##拿到主键
#set($pk = $tableInfo.pkColumn.get(0))

##定义服务名
#set($serviceType = $!tableInfo.name + "Service")

##定义Dao
#set($daoType = $!tableInfo.name + "Dao")
#set($daoName = $!tool.firstLowerCase($!daoType))

##定义实体对象名
#set($entityType = $!tableInfo.name + "Entity")
#set($entityName = $!tool.firstLowerCase($!tableInfo.name))

#set($viewType = $!tableInfo.name + "View")
#set($reqName = $!tool.firstLowerCase($!tableInfo.name) + "Req")

##设置表后缀（宏定义）
#setTableSuffix($!{suffix})

##保存文件（宏定义）
#save("/$!{pkg.replace('.', '/')}", "$!{suffix}.java")

##包路径（宏定义）
#setPackageSuffix($!{pkg})

##表注释（宏定义）
#tableComment("表服务实现类")
@Service
@AllArgsConstructor
public class $!{tableName} implements $!serviceType {

    private $!daoType $!daoName;

    @Override
    public $!entityType findById($!pk.shortType $!pk.name) {
        return $!{daoName}.selectByPrimaryKey($!pk.name);
    }

    @Override
    public $!entityType findOne($!entityType $!entityName) {
        return $!{daoName}.selectOne($!entityName);
    }

    @Override
    public List<$!entityType> findAll($!entityType $!entityName) {
        return $!{daoName}.select($!entityName);
    }

    @Override
    public boolean insert($!entityType $!entityName) {
        return $!{daoName}.insertSelective($!entityName) > 0;
    }

    @Override
    public boolean updateById($!entityType $!entityName) {
        return $!{daoName}.updateByPrimaryKeySelective($!entityName) > 0;
    }

    @Override
    public boolean deleteById($!pk.shortType $!pk.name) {
        return $!{daoName}.deleteByPrimaryKey($!pk.name) > 0;
    }

    @Override
    public boolean insertBatch(List<$!entityType> $!{entityName}s) {
        return $!{daoName}.insertBatch($!{entityName}s) > 0;
    }

    @Override
    public boolean updateBatchById(List<$!entityType> $!{entityName}s) {
        return $!{daoName}.updateBatchById($!{entityName}s) > 0;
    }

    @Override
    public boolean deleteBatchByIds(List<$!pk.shortType> $!{pk.name}s) {
        return $!{daoName}.deleteBatchByIds($!{pk.name}s) > 0;
    }

    @Override
    public List<$!viewType> get$!{tableInfo.name}s($!viewType $!reqName) {
        $!entityType $!entityName = PropertyUtils.copyPropertiesIgnoreNull($!reqName, $!entityType::new);
        List<$!entityType> $!{entityName}s = this.findAll($!entityName);
        return PropertyUtils.copyPropertiesListIgnoreNull($!{entityName}s, $!viewType::new);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save$!{tableInfo.name}($!viewType $!reqName) {
        $!entityType $!entityName = PropertyUtils.copyPropertiesIgnoreNull($!reqName, $!entityType::new);
        this.insert($!entityName);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete$!{tableInfo.name}ById($!pk.shortType $!pk.name) {
        this.deleteById($!pk.name);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update$!{tableInfo.name}ById($!viewType $!reqName) {
        $!entityType $!entityName = PropertyUtils.copyPropertiesIgnoreNull($!reqName, $!entityType::new);
        this.updateById($!entityName);
    }

    @Override
    public $!viewType get$!{tableInfo.name}ById($!pk.shortType $!pk.name) {
        $!entityType $!entityName = this.findById($!pk.name);
        return PropertyUtils.copyPropertiesIgnoreNull($!entityName, $!viewType::new);
    }
}