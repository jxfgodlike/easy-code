package com.easy.code.core.utils;

import com.easy.code.core.consts.Symbol;
import com.easy.code.core.consts.YesOrNo;
import org.apache.commons.io.IOUtils;
import org.jetbrains.java.decompiler.main.decompiler.ConsoleDecompiler;
import org.jetbrains.java.decompiler.main.decompiler.PrintStreamLogger;
import org.jetbrains.java.decompiler.main.extern.IFernflowerLogger;
import org.jetbrains.java.decompiler.main.extern.IFernflowerPreferences;
import org.jetbrains.java.decompiler.util.InterpreterUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

/**
 * 自定义反编译器
 *
 * @author jxfgodlike
 * @date 2022-05-22 16:41
 */
public class JDecompiler extends ConsoleDecompiler {

    private static final String YES = String.valueOf(YesOrNo.YES);
    private static final String NO = String.valueOf(YesOrNo.NO);
    private static final Map<String, Object> OPTIONS = new HashMap<>();
    private static final PrintStreamLogger LOGGER = new PrintStreamLogger(System.out);

    static {
        OPTIONS.put(IFernflowerPreferences.REMOVE_BRIDGE, YES);
        OPTIONS.put(IFernflowerPreferences.REMOVE_SYNTHETIC, YES);
        OPTIONS.put(IFernflowerPreferences.DECOMPILE_INNER, YES);
        OPTIONS.put(IFernflowerPreferences.DECOMPILE_CLASS_1_4, YES);
        OPTIONS.put(IFernflowerPreferences.DECOMPILE_ASSERTIONS, YES);
        OPTIONS.put(IFernflowerPreferences.HIDE_EMPTY_SUPER, YES);
        OPTIONS.put(IFernflowerPreferences.HIDE_DEFAULT_CONSTRUCTOR, YES);
        OPTIONS.put(IFernflowerPreferences.DECOMPILE_GENERIC_SIGNATURES, YES);
        OPTIONS.put(IFernflowerPreferences.NO_EXCEPTIONS_RETURN, YES);
        OPTIONS.put(IFernflowerPreferences.DECOMPILE_ENUM, YES);
        OPTIONS.put(IFernflowerPreferences.REMOVE_GET_CLASS_NEW, YES);
        OPTIONS.put(IFernflowerPreferences.LITERALS_AS_IS, NO);
        OPTIONS.put(IFernflowerPreferences.ASCII_STRING_CHARACTERS, NO);
        OPTIONS.put(IFernflowerPreferences.BOOLEAN_TRUE_ONE, YES);
        OPTIONS.put(IFernflowerPreferences.SYNTHETIC_NOT_SET, NO);
        OPTIONS.put(IFernflowerPreferences.UNDEFINED_PARAM_TYPE_OBJECT, YES);
        OPTIONS.put(IFernflowerPreferences.USE_DEBUG_VAR_NAMES, YES);
        OPTIONS.put(IFernflowerPreferences.USE_METHOD_PARAMETERS, YES);
        OPTIONS.put(IFernflowerPreferences.REMOVE_EMPTY_RANGES, YES);
        OPTIONS.put(IFernflowerPreferences.FINALLY_DEINLINE, YES);
        OPTIONS.put(IFernflowerPreferences.MAX_PROCESSING_METHOD, NO);
        OPTIONS.put(IFernflowerPreferences.RENAME_ENTITIES, NO);
        OPTIONS.put(IFernflowerPreferences.IDEA_NOT_NULL_ANNOTATION, YES);
        OPTIONS.put(IFernflowerPreferences.LAMBDA_TO_ANONYMOUS_CLASS, NO);
        OPTIONS.put(IFernflowerPreferences.NEW_LINE_SEPARATOR, InterpreterUtil.IS_WINDOWS ? NO : YES);
        OPTIONS.put(IFernflowerPreferences.INDENT_STRING, Symbol.TAB);
        OPTIONS.put(IFernflowerPreferences.LOG_LEVEL, IFernflowerLogger.Severity.INFO.name());
    }

    private String qualifiedName;
    private StringBuilder builder;
    private JarFile jar;

    protected JDecompiler(String jarPath, String qualifiedName, Map<String, Object> options, IFernflowerLogger logger) {
        super(null, options, logger);
        this.qualifiedName = qualifiedName.replace(Symbol.CONCAT, Symbol.SLASH);
        this.builder = new StringBuilder();
        this.jar = buildJar(jarPath);
    }

    public static String decompile(String jarPath, String qualifiedName) {
        JDecompiler decompiler = new JDecompiler(jarPath, qualifiedName, OPTIONS, LOGGER);
        decompiler.addSource(new File(jarPath));
        decompiler.decompileContext();
        return decompiler.builder.toString();
    }

    private JarFile buildJar(String jarPath) {
        try {
            return new JarFile(jarPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] getBytecode(String externalPath, String internalPath) throws IOException {
        return IOUtils.toByteArray(jar.getInputStream(jar.getJarEntry(internalPath)));
    }

    @Override
    public void saveFolder(String path) {

    }

    @Override
    public void copyFile(String source, String path, String entryName) {

    }

    @Override
    public void saveClassFile(String path, String qualifiedName, String entryName, String content, int[] mapping) {
        if (qualifiedName.contains(this.qualifiedName)) {
            builder.append(content);
        }
    }

    @Override
    public void createArchive(String path, String archiveName, Manifest manifest) {

    }

    @Override
    public void saveDirEntry(String path, String archiveName, String entryName) {

    }

    @Override
    public void copyEntry(String source, String path, String archiveName, String entryName) {

    }

    @Override
    public synchronized void saveClassEntry(String path, String archiveName, String qualifiedName, String entryName, String content) {
        if (qualifiedName.contains(this.qualifiedName)) {
            builder.append(content);
        }
    }

    @Override
    public void closeArchive(String path, String archiveName) {

    }
}
