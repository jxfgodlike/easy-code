package com.easy.code.validation.metadata.impl;

import com.easy.code.validation.metadata.SimpleConstraintTreeImpl;
import org.hibernate.validator.internal.engine.ValidationContext;
import org.hibernate.validator.internal.engine.ValueContext;
import org.hibernate.validator.internal.engine.constraintvalidation.ConstraintTree;
import org.hibernate.validator.internal.metadata.descriptor.ConstraintDescriptorImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintViolation;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Set;

/**
 * 自定义约束树impl
 *
 * @author jxfgodlike
 * @date 2022-04-16 15:23
 */
public class EasyConstraintTreeImpl<B extends Annotation> extends SimpleConstraintTreeImpl<B> {

    public EasyConstraintTreeImpl(ConstraintDescriptorImpl<B> descriptor, Type validatedValueType) {
        super(descriptor, validatedValueType);
    }

    public static <U extends Annotation> ConstraintTree<U> of(ConstraintDescriptorImpl<U> composingDescriptor, Type validatedValueType) {
        return new EasyConstraintTreeImpl<>(composingDescriptor, validatedValueType);
    }

    @Override
    public <T> void validateConstraints(ValidationContext<T> validationContext, ValueContext<?, ?> valueContext, Set<ConstraintViolation<T>> constraintViolations) {
        if (LOG.isTraceEnabled()) {
            LOG.tracef("Validating value %s against constraint defined by %s.", valueContext.getCurrentValidatedValue(), descriptor);
        }
        ConstraintValidator<B, ?> validator = getInitializedConstraintValidator(validationContext, valueContext);
        EasyConstraintValidatorContextImpl constraintValidatorContext = new EasyConstraintValidatorContextImpl(validationContext.getParameterNames(), validationContext.getClockProvider(), valueContext.getPropertyPath(), descriptor, validationContext.getConstraintValidatorPayload());
        constraintValidatorContext.setTarget(validationContext.getRootBean());
        constraintViolations.addAll(validateSingleConstraint(validationContext, valueContext, constraintValidatorContext, validator));
    }
}
