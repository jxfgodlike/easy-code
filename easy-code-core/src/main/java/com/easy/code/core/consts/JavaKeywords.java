package com.easy.code.core.consts;

/**
 * java关键字
 *
 * @author jxfgodlike
 * @date 2022-06-03 10:57
 */
public class JavaKeywords {

    public static final String JAVA = "java";
    public static final String JAR = "jar";
    public static final String SOURCES = "sources";
    public static final String NULL = "null";
    public static final String NEW = "new";
    public static final String CLASS = "class";
    public static final String THIS = "this";
    public static final String STATIC = "static";
    public static final String SUPER = "super";
    public static final String RETURN = "return";
    public static final String TRY = "try";
    public static final String CATCH = "catch";
    public static final String FINALLY = "finally";
    public static final String THROW = "throw";
    public static final String THROWS = "throws";
    public static final String IF = "if";
    public static final String ELSE = "else";
    public static final String FOR = "for";
    public static final String WHILE = "while";
    public static final String INSTANCEOF = "instanceof";
    public static final String INSTANCE = "INSTANCE";
    public static final String BYTE = "byte";
    public static final String SHORT = "short";
    public static final String INT = "int";
    public static final String LONG = "long";
    public static final String FLOAT = "float";
    public static final String DOUBLE = "double";
    public static final String CHAR = "char";
    public static final String BOOLEAN = "boolean";
}
