package com.easy.code.jdbc.utils;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.easy.code.jdbc.consts.SQLKeywords;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * 查询参数
 *
 * @author jxfgodlike
 * @date 2021/11/25
 */
@Data
public class Query<T> {
    /**
     * 当前页码
     */
    @TableField(exist = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Integer pageNum = 1;
    /**
     * 每页条数
     */
    @TableField(exist = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Integer pageSize = 10;
    /**
     * 排序字段
     */
    @TableField(exist = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String column;
    /**
     * 排序方式
     */
    @TableField(exist = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String orderBy;
    /**
     * 检索关键字
     */
    @TableField(exist = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String searchKey;

    public IPage<T> getPage(Query<T> query) {

        //分页对象
        Page<T> page = new Page<>(query.getPageNum(), query.getPageSize());
        //防止SQL注入（因为orderColumn、orderBy是通过拼接SQL实现排序的，会有SQL注入风险）
        String column = SQLUtils.sqlInject(query.getColumn());
        //前端字段排序(默认升序)
        if (StringUtils.isNotBlank(column)) {
            if (SQLKeywords.DESC.equalsIgnoreCase(query.getOrderBy())) {
                page.addOrder(OrderItem.desc(column));
            } else {
                page.addOrder(OrderItem.asc(column));
            }
        }
        return page;
    }
}
