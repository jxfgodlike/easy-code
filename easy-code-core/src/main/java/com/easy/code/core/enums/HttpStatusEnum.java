package com.easy.code.core.enums;

import com.easy.code.core.utils.CollUtils;

import java.util.Arrays;
import java.util.Map;

/**
 * http状态枚举
 *
 * @author jxfgodlike
 * @date 2021-09-05 0:15
 */
public enum HttpStatusEnum {

    /**
     * 默认
     */
    DEFAULT(-1, "默认"),
    /**
     * 操作成功
     */
    SUCCESS(0, "操作成功"),
    /**
     * 对象创建成功
     */
    CREATED(201, "创建成功"),
    /**
     * 请求已经被接受
     */
    ACCEPTED(202, "请求已经被接受"),
    /**
     * 操作已经执行成功，但是没有返回数据
     */
    NO_CONTENT(204, "没有返回数据"),
    /**
     * 资源已被移除
     */
    MOVED_PERM(301, "资源已被移除"),
    /**
     * 重定向
     */
    SEE_OTHER(303, "重定向"),
    /**
     * 资源没有被修改
     */
    NOT_MODIFIED(304, "资源没有被修改"),
    /**
     * 参数列表错误（缺少，格式不匹配）
     */
    BAD_REQUEST(400, "请求参数错误"),
    /**
     * 未认证
     */
    UNAUTHORIZED(401, "未登录或登录已过期"),
    /**
     * 访问受限，授权过期
     */
    FORBIDDEN(403, "无权限访问"),
    /**
     * 资源，服务未找到
     */
    NOT_FOUND(404, "资源不存在,请检查路径是否正确"),
    /**
     * 不允许的http方法
     */
    METHOD_NOT_ALLOWED(405, "请求方法不支持"),
    /**
     * 请求超时
     */
    REQUEST_TIMEOUT(408, "请求超时，请稍后重试"),
    /**
     * 资源冲突，或者资源被锁
     */
    CONFLICT(409, "请求资源冲突"),
    /**
     * 不支持的媒体数据类型
     */
    UNSUPPORTED_MEDIA_TYPE(415, "不支持的数据类型"),
    /**
     * 资源被锁定
     */
    LOCKED(423, "账号被锁定"),
    /**
     * 用户名或密码错误
     */
    BAD_CREDENTIALS(452, "用户名或密码错误"),
    /**
     * 用户不存在
     */
    //(453, "用户不存在"),
    /**
     * 请勿重复提交
     */
    REPEAT_SUBMIT(454, "请勿重复提交"),
    /**
     * 非法输入
     */
    ILLEGAL_INPUT(455, "非法输入"),
    /**
     * 重复键
     */
    DUPLICATE_KEY(456, "数据已存在"),
    /**
     * 数据不存在
     */
    NO_EXIST(457, "数据不存在"),
    /**
     * 已经修改过
     */
    ALREADY_MODIFIED(458, "数据已修改，请刷新重试"),
    /**
     * 系统内部错误
     */
    ERROR(500, "啊哦，服务器打瞌睡了，再试一次吧~");

    public static final Map<Integer, HttpStatusEnum> HTTP_STATUS_MAP = CollUtils.toMap(Arrays.asList(values()), HttpStatusEnum::code);

    /**
     * 状态码
     */
    private final Integer code;

    /**
     * 消息
     */
    private final String message;

    HttpStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static HttpStatusEnum valueOf(Integer code) {
        return HTTP_STATUS_MAP.getOrDefault(code, DEFAULT);
    }
}
