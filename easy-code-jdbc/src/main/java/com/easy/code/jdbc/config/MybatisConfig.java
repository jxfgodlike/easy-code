package com.easy.code.jdbc.config;

import com.easy.code.jdbc.interceptor.EasySqlInjector;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis配置
 *
 * @author jxfgodlike
 * @date 2022-10-16 13:07
 */
@Configuration
@ConditionalOnClass({MybatisAutoConfiguration.class})
public class MybatisConfig {

    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return configuration -> {
            configuration.addInterceptor(new EasySqlInjector());
        };
    }
}
