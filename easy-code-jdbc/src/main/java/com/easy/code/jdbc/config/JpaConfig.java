package com.easy.code.jdbc.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * jpa配置
 *
 * @author jxfgodlike
 * @date 2022-07-09 15:59
 */
@Configuration
@EnableJpaAuditing
@ConditionalOnClass({JpaRepositoriesAutoConfiguration.class})
public class JpaConfig {

}
