package com.easy.code.jdbc.exception;

import com.easy.code.core.enums.HttpStatusEnum;
import com.easy.code.core.resp.EasyResponseBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author jxfgodlike
 * @date 2022-04-22 23:07
 */
@Slf4j
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class JdbcExceptionHandler {

    @ExceptionHandler(DuplicateKeyException.class)
    public EasyResponseBody<?> error(DuplicateKeyException e) {
        log.warn(e.getMessage(), e);
        return EasyResponseBody.error(HttpStatusEnum.DUPLICATE_KEY);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public EasyResponseBody<?> error(EmptyResultDataAccessException e) {
        log.warn(e.getMessage(), e);
        return EasyResponseBody.error(HttpStatusEnum.NO_EXIST);
    }

    @ExceptionHandler(OptimisticLockingFailureException.class)
    public EasyResponseBody<?> error(OptimisticLockingFailureException e) {
        log.warn(e.getMessage(), e);
        return EasyResponseBody.error(HttpStatusEnum.ALREADY_MODIFIED);
    }
}
