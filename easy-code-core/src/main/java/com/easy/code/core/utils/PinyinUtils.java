package com.easy.code.core.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.apache.commons.lang3.StringUtils;

/**
 * 拼音助手
 *
 * @author jxfgodlike
 * @date 2021/6/14 22:06
 */
public class PinyinUtils {

    private static final String CHINESE_REG_EXP = "[\\u4e00-\\u9fa5]";

    public static String getUpperStr(String name) {
        StringBuilder builder = new StringBuilder();
        if (StringUtils.isNotBlank(name)) {
            for (int i = 0; i < name.length(); i++) {
                String characters = String.valueOf(name.charAt(i));
                builder.append(converterToFirstSpell(characters).toUpperCase().charAt(0));
            }
        }
        return builder.toString();
    }

    public static String converterToFirstSpell(String str) {
        StringBuilder pinyinName = new StringBuilder();
        char[] nameChar = str.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (char characters : nameChar) {
            String string = String.valueOf(characters);
            if (string.matches(CHINESE_REG_EXP)) {
                try {
                    String[] mPinyinArray = PinyinHelper.toHanyuPinyinStringArray(characters, defaultFormat);
                    pinyinName.append(mPinyinArray[0]);
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    throw new RuntimeException(e);
                }
            } else {
                pinyinName.append(characters);
            }
        }
        return pinyinName.toString();
    }
}
