package com.easy.code.jdbc.validation.constraints;

import com.easy.code.jdbc.annotation.Mapping;
import com.easy.code.jdbc.validation.validator.UniqueKeyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 唯一键
 *
 * @author jxfgodlike
 * @date 2022-04-15 21:48
 */
@Documented
@Constraint(validatedBy = {UniqueKeyValidator.class})
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueKey {

    /**
     * 表名
     */
    String table();

    /**
     * 主键列
     */
    String keyColumn();

    /**
     * 校验列
     */
    String[] columns();

    /**
     * where 条件
     */
    String where() default "";

    /**
     * 映射
     */
    Mapping[] mappings() default {};

    String message() default "{javax.validation.constraints.UniqueKey.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
