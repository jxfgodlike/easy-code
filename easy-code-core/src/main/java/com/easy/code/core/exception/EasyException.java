package com.easy.code.core.exception;

import com.easy.code.core.enums.HttpStatusEnum;

/**
 * 简单异常
 *
 * @author jxfgodlike
 * @date 2021/11/24
 */
public class EasyException extends RuntimeException {

    private static final long serialVersionUID = -3419415147800618873L;

    private Integer code;

    private String message;

    public EasyException(String message) {
        this(HttpStatusEnum.ERROR.code(), message);
    }

    public EasyException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public EasyException(HttpStatusEnum httpStatus) {
        this.code = httpStatus.code();
        this.message = httpStatus.message();
    }

    public Integer code() {
        return code;
    }

    public String message() {
        return message;
    }
}
