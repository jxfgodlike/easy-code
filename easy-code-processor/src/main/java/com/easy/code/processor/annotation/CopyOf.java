package com.easy.code.processor.annotation;

import java.lang.annotation.*;

/**
 * 复制类
 *
 * @author jxfgodlike
 * @date 2022-04-23 22:03
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface CopyOf {

    String value();
}
