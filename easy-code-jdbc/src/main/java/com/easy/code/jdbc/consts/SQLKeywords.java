package com.easy.code.jdbc.consts;

/**
 * sql常量
 *
 * @author jxfgodlike
 * @date 2022-04-22 22:25
 */
public class SQLKeywords {

    /**
     * 创建者
     */
    public static final String CREATED_BY = "createdBy";
    /**
     * 创建时间
     */
    public static final String CREATED_DATE = "createdDate";
    /**
     * 更新者
     */
    public static final String UPDATED_BY = "updatedBy";
    /**
     * 更新时间
     */
    public static final String UPDATED_DATE = "updatedDate";
    /**
     * 当前页码
     */
    public static final String PAGE_NUM = "pageNum";
    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";
    /**
     * 排序字段
     */
    public static final String COLUMN = "column";
    /**
     * 排序方式
     */
    public static final String ORDER_BY = "orderBy";
    /**
     * 升序
     */
    public static final String ASC = "ASC";
    /**
     * 降序
     */
    public static final String DESC = "DESC";
    /**
     * 主
     */
    public static final String MASTER = "MASTER";
    /**
     * 声明
     */
    public static final String DECLARE = "DECLARE";
    /**
     * 创建表
     */
    public static final String CREATE = "CREATE";
    /**
     * 删除表
     */
    public static final String DROP = "DROP";
    /**
     * 改变表
     */
    public static final String ALTER = "ALTER";
    /**
     * 清空表
     */
    public static final String TRUNCATE = "TRUNCATE";
    /**
     * 插入
     */
    public static final String INSERT = "INSERT";
    /**
     * 删除
     */
    public static final String DELETE = "DELETE";
    /**
     * 更新
     */
    public static final String UPDATE = "UPDATE";
    /**
     * 查询
     */
    public static final String SELECT = "SELECT";
    /**
     * count
     */
    public static final String COUNT = "COUNT";
    /**
     * count
     */
    public static final String COUNT_STAR = "COUNT(*)";
    /**
     * avg
     */
    public static final String AVG = "AVG";
    /**
     * max
     */
    public static final String MAX = "MAX";
    /**
     * min
     */
    public static final String MIN = "MIN";
    /**
     * from
     */
    public static final String FROM = "FROM";
    /**
     * where
     */
    public static final String WHERE = "WHERE";
    /**
     * and
     */
    public static final String AND = "AND";
    /**
     * or
     */
    public static final String OR = "OR";
    /**
     * <=>
     */
    public static final String EQUALS = "<=>";
    /**
     * <>
     */
    public static final String UNEQUALS = "<>";
    /**
     * is
     */
    public static final String IS = "IS";
    /**
     * not
     */
    public static final String NOT = "NOT";
    /**
     * 限制
     */
    public static final String LIMIT = "LIMIT";
    /**
     * 关键字
     */
    public static final String[] KEYWORDS = {MASTER, DECLARE, CREATE, DROP, ALTER, TRUNCATE, INSERT, DELETE, UPDATE, SELECT, FROM, COUNT, AVG, MAX, MIN, WHERE, AND, OR, EQUALS, UNEQUALS};
}
